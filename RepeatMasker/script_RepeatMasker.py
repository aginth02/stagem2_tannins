#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import sys
import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord

#__________________________________________________________________________________________________________________
#SCRIP_RepeatMasker.py
#Permet de récupérer les séquences des Elements transposable sur le genomes de reference 

#commande : python script_RepeatMasker.py sortie_repeatmasker.out fasta_region_genRef dossier_result/

#sortie_repeatmasker.out : sortie .out de RepeatMasker
#fasta_region_genRef : fichier fasta de la region d'interet du genome de reference
#dossier_result : dossier resultat

#__________________________________________________________________________________________________________________
# fonction recuperant les sequences des Elements transposable sur le genomes de reference 
def tab_rm (out_rm,doss_result,contig_genref):
    rm=pd.read_csv(out_rm,sep=',')
    rm['taille']=abs(rm['query end']-rm['query begin'])
    cl = list(set(rm['repeat class/family']))
    tr = ['LTR/ERVK', 'DNA/hAT-Charlie', 'DNA/TcMar-ISRm11', 'DNA/Maverick', 'LTR/ERVL', 'RC/Helitron', 'DNA/hAT-Ac', 'DNA/PIF-Harbinger', 'LINE/RTE', 'DNA/TcMar', 'DNA/ hAT', 'LINE/RTE-RTE', 'LINE/I-Jockey', 'DNA/IS3EU', 'DNA/MULE-MuDR', 'LTR/Pao', 'LINE/R2', 'LINE/Rex-Babar', 'LTR/Ngaro', 'DNA/Dada', 'LINE/L1-Tx1', 'DNA/hAT-Tip100', 'LTR/Gypsy', 'LTR/ERV1', 'LINE/CR1', 'DNA/CMC-Chapaev', 'DNA/Kolobok-T2', 'DNA/Academ-1', 'DNA/Ginger-1', 'LINE/L1', 'DNA/CMC-EnSpm', 'LTR/ERVL-MaLR', 'LTR/Copia']
    
    rm2=rm[rm['taille']>50]
    sp= list(set(rm['query sequence']))
    d=pd.DataFrame(index=sp,columns=['element transp'])
    d_ligne=dict()
    for i in sp : 
        tab = rm2[rm2['query sequence']==i]
        d['element transp'][i]=0
        d_ligne[i]=[]
        for j in tab.index:
            if tab['repeat class/family'][j] in tr:
                d_ligne[i].append(j)
                d['element transp'][i]+=1
    

    fout_name=doss_result+'seq_ET_genref.fasta'
    
    li=[]
    for i in tr :
        tab=rm2[rm2['repeat class/family']==i]
        if len(tab)>0:
            tab=tab.sort_values(by = ['taille'],ascending=[False])
            li.append(tab.index[0])
    
    rm2 = rm2.loc[li,:]
    rm2=rm2.sort_values(by = ['taille'],ascending=[False])

    fout = open(fout_name,'w')
    dico_seq=SeqIO.to_dict(SeqIO.parse(contig_genref,'fasta'))
    for i in dico_seq.keys():
        for j in rm2.index:
            start=rm2['query begin'][j]
            end=rm2['query end'][j]
            name=rm2['matching repeat'][j]
            H = str(dico_seq[i].seq)
            H = H[start:end+1]
            record = SeqRecord(Seq(H),id=name)
            fout.write(record.format('fasta'))
    fout.close()
    print('Recupération des Element transposable terminé' )
    return 


#__________________________________________________________________________________________________________________
# MAIN
def main():
    out_rm = sys.argv[1]
    contig_genref=sys.argv[2]
    doss_result=sys.argv[3]
    tab_rm (out_rm,doss_result,contig_genref)

if __name__ == '__main__':
    main()
