#!/usr/bin/env ruby
#   SyntenyDrawer.rb - Version 1.3c
#   Generates an analysis figure given a path to one or more valid CMAP files as input.
#
#   Code Copyright (C) 2019 Daniel Veltri (dan.veltri@gmail.com)
#
#   This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#   SimpleSynteny Copyright (C) 2019 Daniel Veltri
#   This program comes with ABSOLUTELY NO WARRANTY; This is free software, and you are
#   welcome to redistribute it under certain conditions; for details see the LICENSE.txt
#   file that should have been distributed with this script. 
#===============================================
# SyntenyDrawer.rb by Dan Veltri (dan.veltri@gmail.com)
# Created on 9.18.2014, Last Modified on 4.6.2019
# See Manual PDF for recent modifications.
#===============================================
# Basic Usage: ruby SyntenyDrawer.rb <path_to_cmap_files> [options]
#
# Dependencies: ImageMagick, RMagick GEM.
# See the SimpleSynteny Command Line Manual for more details.
#===============================================

#==================================================
#REQUIRED LIBRARIES
require 'rmagick'
include Magick

#==================================================
#GLOBAL VARIABLES AND USER ARGUMENTS
$VERSION='1.3c'

#Set Argument Defaults
$CMAP_DIR = Dir.pwd
$IMG_WIDTH = 2600 # pixels
$IMG_HEIGHT = 2000 # pixels
$DPI = 600
$SAVED_IMAGE_NAME = "SimpSynFig.png" # must include extension .png, .jpeg, .gif etc.
$TEXT_SIZE = 28 # size of genome name font
$TEXT_GNAME_SIZE = 14 # gene name size
$TEXT_CNAME_SIZE = 20 # contig + bp# size
$GNAME_BUFFER_PERCENT = (1.0 / 100) # (adjust space between genome name and first contig as %, can be negtaive)
$CONTIG_HEIGHT = 50
$CONTIG_OPTIMIZE = "no" # no / dist / flip
$SHADE_HITS = "blast" # full / both / blast
$CONNECT_STYLE = "topdown" # connect genes style: 'all' (all-to-all) or 'topdown' (nearest genome)
$LINETYPE = "curved" # straight / curved
$ARROW_SPACE = 30 # distance to move overlapping arrows
$ALTERNATE_LABELS = true # true / false (alternate labels top/bottom on a contig)
$GENE_BOX = true # true / false (draw white box around gene names)
$GNAME_TEXT_REMOVE = false # true / false (remove gene name text to fill in with an external program)
$GRAYSCALE_MODE = false # true / false
$COLOR_LIST = "./color_list.txt" # optional, will be created if not present
$DEBUG = false # true /false (toggle debug mode text output)


#Usage Message
print_help = false
ARGV.each {|arg| if arg =~ /help/i then print_help = true end}
 puts "\n================================================================================================"
 puts "SimpleSynteny Drawer Vr.#{$VERSION} - Reads CMAP files from a given directory and produces a figure."
 puts "\nPlease Cite: Veltri, D., Malapi-Wight, M. and Crouch J.A. SimpleSynteny: a web-based tool for"
 puts "visualization of microsynteny across multiple species. Nucleic Acids Research, 44(W1) 2016."
 puts "------------------------------------------------------------------------------------------------"
if print_help == true
 puts "\nProgram Usage: ruby SyntenyDrawer.rb <PATH_TO_CMAP_FOLDER> [option flags]\n"
 puts "\n*You did not provide a path to a directory containing valid CMAP files!*" if ARGV.length < 1
 puts "\nOption Flags:"
 puts "-o | -out <string>\t\tName of figure including image extension (default='SimpSynFig.png')"
 puts "\t\t\t\tAvailable image extensions: .png .jpg .gif .eps .tiff .pdf"
 puts "-width <int>\t\t\tWidth of image in pixels (default=2600)"
 puts "-height <int>\t\t\tHeight of image in pixels (default=2000)"
 puts "-dpi | -d <int>\t\t\tDots Per Inch of image (default=600)"
 puts "-space | -gname-space <float>\tOffset buffer between genome name and first contig as +/- percent of image width (default=1.0)"
 puts "-genome-font <int>\t\tGenome name font size (default=28)"
 puts "-gene-font <int>\t\tGene name font size (default=14)"
 puts "-contig-font <int>\t\tContig name font size (default=20)"
 puts "-contig-height | -cheight <int>\tHeight of contig in pixels (default=50)"
 puts "-optimize | -opt <no|dist|flip>\tOptimize contig order relative to first genome? (default='no')"
 puts "-shade <blast|both|full>\tHow to shade gene boxes (default='blast')"
 puts "-connect <topdown|all>\t\tConnect genes top-down to nearest genome or 'all-to-all' (default='topdown')"
 puts "-lines <straight|curved>\tStyle of lines connecting genes (default='curved')"
 puts "-arrows <int>\t\t\tDistance to move overlapping arrows in pixels (default=30)"
 puts "-alt | -alt-labels <true|false>\tAlternate gene labels on the same contig? (default='true')"
 puts "-box | -b <true|false>\t\tDraw a white box around gene names? (default='true')"
 puts "-no-gname | -n <true|false>\tIgnore displaying text for gene names? (default='false', often used with -box set to false)"
 puts "-grayscale | -g <true|false>\tMake figure in grayscale? (default='false')"
 puts "-colors <dir/color_list.txt>\tFull path to optional color_list.txt file to assign gene colors (default=random assignment)"
 puts "\t\t\t\tcolor_list.txt requires 'GENE_NAME::GENE_COLOR', each on a separate line,\n\t\t\t\twhere GENE_COLOR is a six-digit sRGB value: #rrggbb. See Manual.pdf."  
 puts "-debug\t\t\t\tPrints debug output as figure is generated"
 puts "-help\t\t\t\tShow this menu"
 puts "\nAn example to generate a 300DPI, 1200x800px .TIFF figure named MyFig.tiff with straight lines and no white box around gene names:"
 puts "ruby SyntenyDrawer_Vr#{$VERSION}.rb my_cmap_directory/ -width 1200 -height 800 -dpi 300 -box false -lines straight -out MyFig.tiff"
 puts "\n**This program comes with no warranty**\nPlease see the attached documentation for CMAP file format details."
 puts "Contact Dan Veltri (dan.veltri@gmail.com) if you have problems with the program.\n\n"
 exit
end

#Parse Args
if ARGV.length > 0 && ARGV[0] !~ /^-/ && Dir.exists?(ARGV[0]) then $CMAP_DIR = ARGV[0] else puts "Using current working directory..." end # Directory w/ CMAP files
ARGV.each_index do |i|
  if ARGV[i] =~ /^-wi?d?t?h?/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $IMG_WIDTH = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-height/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $IMG_HEIGHT = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-dpi?/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $DPI = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-opti?m?i?z?e?/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /(no|dist|flip)/i then $CONTIG_OPTIMIZE = ARGV[i + 1].downcase end
  if ARGV[i] =~ /^-(arrows?)/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $ARROW_SPACE = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-outp?u?t?/i && ARGV.length >= (i + 1)
    if ARGV[i + 1] =~ /.*\.[jpegntifdsv]+/i
      $SAVED_IMAGE_NAME = ARGV[i + 1]
    else
      puts "\nYour output figure name must include a valid image extension: .png .jpg .gif .eps .tiff or .pdf"
      puts "See RMagick reference for trying additonal extensions (https://rmagick.github.io/)" 
      puts "Use the '-help' flag to see more details on how to run the program.\n"
      exit
    end
  end
  if ARGV[i] =~ /^-genome-font/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $TEXT_SIZE = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-gene-font/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $TEXT_GNAME_SIZE = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-contig-font/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $TEXT_CNAME_SIZE = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-g?n?a?m?e?-?space/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /[0-9\.\-]+/ then $GNAME_BUFFER_PERCENT = (ARGV[i + 1].to_f / 100.0) end
  if ARGV[i] =~ /^-co?n?t?i?g?-?height/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /\d+/ then $CONTIG_HEIGHT = ARGV[i + 1].to_i end
  if ARGV[i] =~ /^-shade/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /(blast|both|full)/i then $SHADE_HITS = ARGV[i + 1].downcase end
  if ARGV[i] =~ /^-connect/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /(topdown|all)/i then $CONNECT_STYLE = ARGV[i + 1].downcase end
  if ARGV[i] =~ /^-li?n?e?s?/i && ARGV.length >= (i + 1) && ARGV[i + 1] =~ /(straight|curved)/i then $LINETYPE = ARGV[i + 1].downcase end
  if ARGV[i] =~ /^-alt-?l?a?b?e?l?s?/i && ARGV.length >= (i + 1)
    if ARGV[i + 1] =~ /tr?u?e?/i then $ALTERNATE_LABELS = true elsif ARGV[i + 1] =~ /fa?l?s?e?/i then $ALTERNATE_LABELS = false end
  end    
  if ARGV[i] =~ /^-bo?x?/i && ARGV[i + 1] !~ /tr?u?e?/i then $GENE_BOX = false end
  if ARGV[i] =~ /^-no?-?g?n?a?m?e?s?/i && ARGV[i + 1] !~ /fa?l?s?e?/i then $GNAME_TEXT_REMOVE = true end
  if ARGV[i] =~ /^-gre?a?y?s?c?a?l?e?/i && ARGV[i + 1]  !~ /fa?l?s?e?/i then $GRAYSCALE_MODE = true end
  if ARGV[i] =~ /^-colou?rs?/i && ARGV.length >= (i + 1)
    if ARGV[i + 1] =~ /.*colou?r\_list\.txt/i
      $COLOR_LIST = ARGV[i + 1]
    else
      puts "\nYou must provide a path to a file name 'color_list.txt' to assign colors!"
      puts "Use the '-help' flag for more details.\n"
      exit
    end
  end
  if ARGV[i] =~ /^-debu?g?/i then $DEBUG = true end
end

#DONT CHANGE THESE SETTINGS!
$DISPLAY_MODE = 'genes' #display width proportional to 'contigs' vs 'genes' - contig setting has been removed/depricated
$last_x = 0 #global tmp variable to prevent drawing labels on top of each other etc.

#==================================================
#DATA TYPES


#CONIG and GENE STRUCTURES
#Genome_Number: Number/Level of genome (as genomes might reuse contig numbers/names) 
#Name: name of contig/gene
#BP_Start: Starting base position for drawing
#BP_End: Ending base position for drawing
#Height: Height of rectangle for contig outline
#Width: Width of rectangle for contig outline
#Disp_X: Upper-left X coord to start drawing
#Disp_Y: Upper-left Y coord to start drawing 
#BGColor: Color to fill
#BP_Complete: Are the base positions the FULL size of the contig?
#  all = bp_start and bp_end is full contig length
#  none = both size incomplete
#  left = bp_start is start# of contig
#  right = bp_end is end# of contig
#  partial_left = partial contig, starts 
#NGENES: number of genes on contig split
#FULL_CONTIG_END: final bp of contig prior to splitting 
Contig = Struct.new(:genome_number, :name, :bp_start, :bp_end, :height, :width, :disp_x, :disp_y, :bgcolor, :bp_complete, :ngenes, :full_contig_end)
Gene = Struct.new(:genome_number, :contig, :name, :bp_start, :bp_end, :bp_seg_start, :bp_seg_end, :dir)


#==================================================
#HELPER FUNCTIONS

#On Entry: Valid Contig Struct is passed, dropshadow is drawn? (true/false), style type of contig end (currently only 'jagged' supported)
#On Exit: Magick::Draw.new is returned based on contig data. Apply with draw(canvas).
def draw_contig(contig, dropshadow=true, end_stype='jagged')
  draw_contig_names = true
  bp_num_offsets = 20 #offset down from bottom of contig rectangle
  
  #ensure nice even numbers for clearer drawing
  contig.disp_x = contig.disp_x.floor 
  contig.width = contig.width.ceil
  
  mark = Magick::Draw.new
  mark.text_align(CenterAlign)
  mark.stroke_width(0.75)

  #Contig name
  if draw_contig_names == true
    if contig.name =~ /(.*)(\.1$)/
      this_cname = $1
      mark.font_family = "Arial"
      mark.font_weight = NormalWeight
      mark.pointsize = $TEXT_CNAME_SIZE
      mark.stroke = "none"
          
      #Draw white box around text if needed
      mark.fill('white')
      mark.fill_opacity(0.8)
      txt_half_width = (mark.get_type_metrics(this_cname).width * 0.5)
      txt_half_height = (mark.get_type_metrics(this_cname).height * 0.5)
      mark.polyline(
        (contig.disp_x - txt_half_width) + 1, (contig.disp_y - (txt_half_height * 2).floor),
        (contig.disp_x + txt_half_width) - 1, (contig.disp_y - (txt_half_height * 2).floor),
        (contig.disp_x + txt_half_width) - 1, (contig.disp_y - 1),
        (contig.disp_x - txt_half_width) + 1, (contig.disp_y - 1),
        (contig.disp_x - txt_half_width) + 1, (contig.disp_y - (txt_half_height * 2).floor)
      )
      mark.fill('black')
      mark.fill_opacity(1)
      mark.text(contig.disp_x, contig.disp_y - (txt_half_height / 2).floor, this_cname) 
    end
  end
  mark.pointsize = $TEXT_CNAME_SIZE - 5

  #Start BP Label
  txt_half_width = (mark.get_type_metrics(contig.bp_start.to_s).width * 0.5)
  txt_half_height = (mark.get_type_metrics(contig.bp_start.to_s).height * 0.4)
  
  #Prevent collision 
  if (contig.disp_x - txt_half_width - 2) < $last_x && $last_x < (contig.disp_x + txt_half_width + 2)
    bp_offset = bp_num_offsets + (txt_half_height * 2)
    puts "WARNING: When drawing Contig #{contig.name}, start BP label collision detected! Shifting height to #{bp_offset}!" if $DEBUG == true
    mark.stroke('gray')
    if contig.disp_x > $last_x then mark.fill_opacity(0.6) else mark.fill_opacity(0.15) end
    mark.line(contig.disp_x, contig.disp_y + contig.height, contig.disp_x, contig.disp_y + contig.height + bp_offset - (txt_half_height).floor) 
  else
    bp_offset = bp_num_offsets
  end
  mark.stroke('none')
  mark.fill('white')
  mark.fill_opacity(0.8)
  
  mark.polyline(
    (contig.disp_x - txt_half_width - 2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor),
    (contig.disp_x + txt_half_width + 2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor),
    (contig.disp_x + txt_half_width + 2), (contig.disp_y - 5 + contig.height + bp_offset + (txt_half_height).floor),
    (contig.disp_x - txt_half_width - 2), (contig.disp_y - 5 + contig.height + bp_offset + (txt_half_height).floor),
    (contig.disp_x - txt_half_width -2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor)
  )
  mark.fill('black')
  mark.fill_opacity(1)
  mark.stroke('none')
  mark.pointsize = $TEXT_CNAME_SIZE - 5
  mark.text(contig.disp_x, contig.disp_y + contig.height + bp_offset, contig.bp_start.to_s)

  #End BP Label
  txt_half_width = (mark.get_type_metrics(contig.bp_end.to_s).width * 0.5)
  txt_half_height = (mark.get_type_metrics(contig.bp_end.to_s).height * 0.4)

  #Prevent collision 
  if (contig.disp_x + contig.width - txt_half_width - 2) < $last_x && $last_x < (contig.disp_x + contig.width + txt_half_width + 2)
    bp_offset = bp_num_offsets + (txt_half_height * 2)
    puts "WARNING: When drawing Contig #{contig.name}, end BP label collision detected! Shifting height to #{bp_offset}!" if $DEBUG == true
    mark.stroke('gray')
    if contig.disp_x > $last_x then mark.fill_opacity(0.6) else mark.fill_opacity(0.15) end
    mark.line(contig.disp_x, contig.disp_y + contig.height, contig.disp_x, contig.disp_y + contig.height + bp_offset - (txt_half_height).floor)
  else
    bp_offset = bp_num_offsets
  end
  mark.fill('white')
  mark.stroke('none')
  mark.fill_opacity(0.8)
  
  mark.polyline(
    (contig.disp_x + contig.width - txt_half_width - 2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor),
    (contig.disp_x + contig.width + txt_half_width + 2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor),
    (contig.disp_x + contig.width + txt_half_width + 2), (contig.disp_y - 5 + contig.height + bp_offset + (txt_half_height).floor),
    (contig.disp_x + contig.width - txt_half_width - 2), (contig.disp_y - 5 + contig.height + bp_offset + (txt_half_height).floor),
    (contig.disp_x + contig.width - txt_half_width - 2), (contig.disp_y - 6 + contig.height + bp_offset - (txt_half_height).floor)
  )
  
  $last_x = contig.disp_x + contig.width + txt_half_width + 2
  mark.fill('black')
  mark.fill_opacity(1)
  #mark.text_align(LeftAlign)
  mark.text((contig.disp_x + contig.width),(contig.disp_y + contig.height + bp_offset),contig.bp_end.to_s)
  mark.stroke('black')
  mark.stroke_opacity(1)
  mark.fill(contig.bgcolor)
  mark.fill_opacity(1)
  mark.stroke_width(0.75)
  
  #Top left -> Top right -> Bottom right -> Bottom left -> Top right
  mark.polyline(contig.disp_x, contig.disp_y, (contig.disp_x + contig.width), contig.disp_y, (contig.disp_x + contig.width), (contig.disp_y + contig.height), contig.disp_x, (contig.disp_y + contig.height),contig.disp_x, contig.disp_y)
  
  #Adjust ends if incomplate
  case contig.bp_complete
    
  when "all"
    #Restore Ends
    mark.line(contig.disp_x, contig.disp_y + 1, contig.disp_x, contig.disp_y + contig.height - 1)
    mark.line(contig.disp_x + contig.width, contig.disp_y + 1, contig.disp_x + contig.width, contig.disp_y + contig.height - 1)

  when "none"
    mark.polyline(contig.disp_x + contig.width,contig.disp_y, contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x + contig.width, (contig.disp_y + (contig.height * 0.5)), contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x + contig.width, (contig.disp_y + contig.height)) if end_stype == 'jagged'
    mark.polyline(contig.disp_x,contig.disp_y, contig.disp_x - 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x, (contig.disp_y + (contig.height * 0.5)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x, (contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "left"
    mark.polyline(contig.disp_x,contig.disp_y, contig.disp_x - 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x, (contig.disp_y + (contig.height * 0.5)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x, (contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "right"
    mark.polyline(contig.disp_x + contig.width,contig.disp_y, contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x + contig.width, (contig.disp_y + (contig.height * 0.5)), contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x + contig.width, (contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "partial_left"
    mark.polyline(contig.disp_x,contig.disp_y, contig.disp_x - 5,contig.disp_y, contig.disp_x, (contig.disp_y + (contig.height * 0.25)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.5)), contig.disp_x, (contig.disp_y + (contig.height * 0.75)), contig.disp_x - 5, (contig.disp_y + contig.height), contig.disp_x, (contig.disp_y + contig.height)) if end_stype == 'jagged'
    mark.fill('none')
    mark.polyline(contig.disp_x - 5 - 5,contig.disp_y, contig.disp_x - 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x - 5 - 5, (contig.disp_y + (contig.height * 0.5)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x - 5 - 5, (contig.disp_y + contig.height)) if dropshadow== false && end_stype == 'jagged'

  when "partial_right"
    mark.polyline(contig.disp_x,contig.disp_y, contig.disp_x - 5,contig.disp_y, contig.disp_x, (contig.disp_y + (contig.height * 0.25)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.5)), contig.disp_x, (contig.disp_y + (contig.height * 0.75)), contig.disp_x - 5, (contig.disp_y + contig.height), contig.disp_x, (contig.disp_y + contig.height)) if end_stype == 'jagged'
    mark.polyline(contig.disp_x + contig.width,contig.disp_y, contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x + contig.width, (contig.disp_y + (contig.height * 0.5)), contig.disp_x + contig.width + 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x + contig.width, (contig.disp_y + contig.height)) if end_stype == 'jagged'
    mark.fill('none')
    mark.polyline(contig.disp_x - 5 - 5,contig.disp_y, contig.disp_x - 5, (contig.disp_y + (contig.height * 0.25)), contig.disp_x - 5 - 5, (contig.disp_y + (contig.height * 0.5)), contig.disp_x - 5, (contig.disp_y + (contig.height * 0.75)), contig.disp_x - 5 - 5, (contig.disp_y + contig.height)) if dropshadow== false && end_stype == 'jagged'

  else
    puts "WARNING from draw_contig: invalid contig.bp_complete string encountered. Skipping annotation!" if $DEBUG == true
  end
  
  return mark  
end

#Helper-Function for draw_contig, draws dropshadow behind contig when called from draw_contig. Only 'jagged' contig end style type currently supported.
#If desired user can manually tweak the shadow effects like how much it is offset or the opacity etc.
def draw_contig_dropshadow(contig, end_stype = 'jagged')
  
  puts "Drawing contig #{contig.name} dropshadow." if $DEBUG == true

  mark = Magick::Draw.new
  mark.stroke('gray')
  mark.stroke_opacity(0.3)
  mark.fill_opacity(0.6)
  mark.fill('gray')
  mark.stroke_width(0.75)
  pad = 2 #offset for shaddow
  hatch_slope = (0.25 * contig.height) / 5.0 #hatch background line slop between contigs
  hatch_dist = pad * 4 #hatch background line slop between contigs
  mark.push #save draw settings

  #Top left -> Top right -> Bottom right -> Bottom left -> Top right
  mark.polyline(contig.disp_x + pad, contig.disp_y + pad, (contig.disp_x + contig.width + pad), contig.disp_y + pad, (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height + pad), contig.disp_x + pad, (contig.disp_y + contig.height + pad),contig.disp_x + pad, contig.disp_y + pad)
  
  case contig.bp_complete
  when "all"
    #skip annotation
  when "none"
    #left-side hash mark
    mark.polyline(pad + contig.disp_x + contig.width,pad + contig.disp_y, pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.25)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + (contig.height * 0.5)), pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.75)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + contig.height)) if end_stype == 'jagged'
    #right-side hash mark
    mark.polyline(pad + contig.disp_x, pad + contig.disp_y, pad + contig.disp_x - 5, (pad + contig.disp_y + (contig.height * 0.25)), pad + contig.disp_x, (pad + contig.disp_y + (contig.height * 0.5)), pad + contig.disp_x - 5, (pad + contig.disp_y + (contig.height * 0.75)), pad + contig.disp_x, (pad + contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "left"
    mark.polyline(contig.disp_x + pad, contig.disp_y + pad, (contig.disp_x + contig.width + pad), contig.disp_y + pad, (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height + pad), contig.disp_x + pad, (contig.disp_y + contig.height + pad),contig.disp_x + pad, contig.disp_y + pad)
    mark.polyline(pad + contig.disp_x,pad + contig.disp_y, pad + contig.disp_x - 5, (pad + contig.disp_y + (contig.height * 0.25)), pad + contig.disp_x, (pad + contig.disp_y + (contig.height * 0.5)), pad + contig.disp_x - 5, (pad + contig.disp_y + (contig.height * 0.75)), pad + contig.disp_x, (pad + contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "right"
    mark.polyline(pad + contig.disp_x + contig.width, pad + contig.disp_y, pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.25)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + (contig.height * 0.5)), pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.75)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + contig.height)) if end_stype == 'jagged'

  when "partial_left"
    mark.fill(contig.bgcolor)
    mark.fill_opacity(0.8)
    mark.stroke('none')
    mark.polyline(($last_x + pad - 1), contig.disp_y, contig.disp_x, contig.disp_y, contig.disp_x, (contig.disp_y + contig.height + pad), ($last_x + pad), (contig.disp_y + contig.height + pad), ($last_x + pad + 5), (pad + contig.disp_y + (contig.height * 0.75)), ($last_x + pad), (pad + contig.disp_y + (contig.height * 0.5)), ($last_x + pad + 5), (pad + contig.disp_y + (contig.height * 0.25)), $last_x + pad - 1, contig.disp_y)
    
    #draw hatch marks
    mark.fill('gray')
    mark.stroke_width(1)
    mark.fill_opacity(0.7)
    last_bot_x = contig.disp_x + 1 + (contig.height / hatch_slope)
    ($last_x .. last_bot_x).step(hatch_dist) do |bot_x|
      top_x = (( contig.height / hatch_slope) - bot_x) * -1.0
      mark.line(top_x, contig.disp_y, bot_x, (contig.disp_y + contig.height))
    end

    mark.pop #restore
    
    #draw shadow under section between contigs
    mark.polyline($last_x + pad, (contig.disp_y + contig.height), (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height), (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height + pad), $last_x + pad, (contig.disp_y + contig.height + pad), $last_x + pad, contig.disp_y + pad)

  when "partial_right"
    #draw background between contig segments
    mark.fill(contig.bgcolor)
    mark.fill_opacity(0.8)
    mark.stroke('none')
    mark.polyline(($last_x + pad - 1), contig.disp_y, contig.disp_x, contig.disp_y, contig.disp_x, (contig.disp_y + contig.height + pad), ($last_x + pad), (contig.disp_y + contig.height + pad), ($last_x + pad + 5), (pad + contig.disp_y + (contig.height * 0.75)), ($last_x + pad), (pad + contig.disp_y + (contig.height * 0.5)), ($last_x + pad + 5), (pad + contig.disp_y + (contig.height * 0.25)), $last_x + pad - 1, contig.disp_y)

    #draw hatch marks
    mark.fill('gray')
    mark.stroke_width(1)
    mark.fill_opacity(0.7)
    last_bot_x = contig.disp_x + 1 + (contig.height / hatch_slope)
    ($last_x .. last_bot_x).step(hatch_dist) do |bot_x|
      top_x = (( contig.height / hatch_slope) - bot_x) * -1.0
      mark.line(top_x, contig.disp_y, bot_x, (contig.disp_y + contig.height))
    end

    mark.pop #restore
    
    #draw shadow under section between contigs
    mark.polyline($last_x + pad, (contig.disp_y + contig.height), (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height), (contig.disp_x + contig.width + pad), (contig.disp_y + contig.height + pad), $last_x + pad, (contig.disp_y + contig.height + pad), $last_x + pad, contig.disp_y + pad)

    #righ-side edge shadow
    mark.polyline(pad + contig.disp_x + contig.width, pad + contig.disp_y, pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.25)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + (contig.height * 0.5)), pad + contig.disp_x + contig.width + 5, (pad + contig.disp_y + (contig.height * 0.75)), pad + contig.disp_x + contig.width, (pad + contig.disp_y + contig.height)) if end_stype == 'jagged'

  else
    puts "WARNING from draw_contig: invalid contig.bp_complete string '#{contig.bp_complete}' encountered. Skipping drop shadow annotation!" if $DEBUG == true
  end
  
  $last_x = contig.disp_x + contig.width
  
  return mark
end

#Draw the gene boxes and names on a contig
#On Entry: Valid Contig Struct, gene_name, starting bp of gene, arrow width and direction is provided.
# Global bool $ALTERNATE_LABELS overrides alternating top/bottom drawing style of genes which is default, $GENE_BOX draws a white box around gene names
#On Exit: Magick::Draw.new is returned based on contig data. Apply with draw(canvas).
def draw_gene(contig, gene_name, gene_bp_start, gene_bp_end, gene_segbp_start, gene_segbp_end, arrow_width, gene_color='white', direction='right', gcount=0, location='full')
  
  draw_gene_name_color = false # will draw gene names with color of gene box if turned on - can makes things a bit hard to read.
  
  #Convert scale
  if $SHADE_HITS == 'blast'
    start_segment_x = convert_range(x=gene_segbp_start, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
    end_segment_x = convert_range(x=gene_segbp_end, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
    start_x = start_segment_x
    end_x = end_segment_x
  else # base on full-length gene
    start_x = convert_range(x=gene_bp_start, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
    end_x = convert_range(x=gene_bp_end, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
    start_segment_x = convert_range(x=gene_segbp_start, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
    end_segment_x = convert_range(x=gene_segbp_end, contig.bp_end, contig.bp_start,(contig.disp_x + contig.width), contig.disp_x)
  end
  
  puts "CONTIG BP END: #{contig.bp_end}","FULL GENE START: #{start_x}","FULL GENE END: #{end_x}", "SEG GENE START: #{start_segment_x}","SEG GENE END: #{end_segment_x}" if $DEBUG == true
  puts "    Drawing gene #{gene_name} on contig #{contig.name} at X:#{start_x}-#{end_x},(Shade X:#{start_segment_x}-#{end_segment_x}) Y:#{contig.disp_y}, DIR: #{direction}." if $DEBUG == true
  puts "    Original BP Coords Given- #{gene_bp_start}-#{gene_bp_end} (Shade #{gene_segbp_start}-#{gene_segbp_end})" if $DEBUG == true

  #Sanity Checks
  #DEBUG raise "ERROR from draw_gene: gene base #{gene_bp_start} doesn't fit on contig #{contig.name}! [#{contig.bp_start},#{contig.bp_end}]!" if gene_bp_start > contig.bp_end || gene_bp_start < contig.bp_start
  puts "WARNING from draw_gene: gene arrow slides off right side of contig #{contig.name}!" if gene_bp_end > contig.bp_end && direction == 'right' && $DEBUG == true
  puts "WARNING from draw_gene: gene arrow slides off left side of contig #{contig.name}!" if gene_bp_start < contig.bp_start && direction == 'left' && $DEBUG == true

  mark = Magick::Draw.new
  mark.fill_opacity(0.8)
  mark.pointsize = $TEXT_GNAME_SIZE #15
  mark.text_align(CenterAlign) 

  if location == 'full' && (gcount % 2 == 0 || $ALTERNATE_LABELS == false) #draw style full with text on bottom
    mark.text_align(CenterAlign) 
    txt_center = start_x + ((end_x - start_x + 1) * 0.5)
      
    #Draw white box around text if needed
    mark.font_style(ItalicStyle)
    mark.fill('white')
    txt_width = mark.get_type_metrics(gene_name.upcase).width.ceil

    if $GENE_BOX == true
      mark.fill_opacity(0.8)
      mark.polyline(
        txt_center - 2 - (0.5 * txt_width),(contig.disp_y + (0.66 * contig.height).ceil + 1),
        txt_center + 2 + (0.5 * txt_width), (contig.disp_y + (0.66 * contig.height).ceil + 1),
        txt_center + 2 + (0.5 * txt_width), (contig.disp_y + contig.height - 1),
        txt_center - 2 - (0.5 * txt_width), (contig.disp_y + contig.height - 1),
        txt_center - 2 - (0.5 * txt_width), (contig.disp_y + (0.66 * contig.height).ceil + 1)
      )      
    end
        
    #Draw gene name
    if($GNAME_TEXT_REMOVE == false)
      puts "Color Used: #{gene_color}" if $DEBUG == true
      mark.fill(gene_color)
      mark.fill_opacity(1)
      mark.text(txt_center + 1, (contig.disp_y + (0.66 * contig.height).ceil) + (0.3 * contig.height), gene_name.upcase)  if draw_gene_name_color == true 
      mark.fill('black')
      mark.text(txt_center, (contig.disp_y + (0.66 * contig.height).ceil) + (0.3 * contig.height), gene_name.upcase)
    else
      puts "Skipping name/text for: #{gene_name.upcase}." if $DEBUG == true
    end

    #Draw only BLAST hit segment
    if $SHADE_HITS == 'both'
      mark.stroke('black')
      mark.stroke_opacity(0.4)
      mark.fill(gene_color)
      mark.fill_opacity(0.5)
      if direction == 'right'
        if (end_segment_x - start_segment_x + 1) < (arrow_width * 2) #if too short just draw triangle
          mark.polyline(start_segment_x, contig.disp_y, end_segment_x, contig.disp_y + (0.33 * contig.height), start_segment_x, contig.disp_y + (0.66 * contig.height), start_segment_x, contig.disp_y)
        else
          mark.polyline(start_segment_x, contig.disp_y, (end_segment_x - arrow_width), contig.disp_y, end_segment_x, contig.disp_y + (0.33 * contig.height).ceil, (end_segment_x - arrow_width), contig.disp_y + (0.66 * contig.height).floor, start_segment_x, contig.disp_y + (0.66 * contig.height).floor, start_segment_x, contig.disp_y)
        end
      elsif direction == 'left'
        if (end_segment_x - start_segment_x + 1) < (arrow_width * 2) #if too short just draw triangle
          mark.polyline(end_segment_x, contig.disp_y, start_segment_x, contig.disp_y + (0.33 * contig.height), end_segment_x, contig.disp_y + (0.66 * contig.height), end_segment_x, contig.disp_y)
        else
          mark.polyline(end_segment_x, contig.disp_y, (start_segment_x + arrow_width), contig.disp_y, start_segment_x, contig.disp_y + (0.33 * contig.height).ceil, (start_segment_x + arrow_width), contig.disp_y + (0.66 * contig.height).floor, end_segment_x, contig.disp_y + (0.66 * contig.height).floor, end_segment_x, contig.disp_y)
        end
      else #square
        mark.polyline(end_segment_x, contig.disp_y.ceil, start_segment_x, contig.disp_y.ceil, start_segment_x, contig.disp_y + (0.66 * contig.height).ceil, end_segment_x, contig.disp_y + (0.66 * contig.height).ceil, end_segment_x, contig.disp_y.ceil)  
      end
    end

    #Draw main full-size gene box
    mark.stroke('black')
    mark.stroke_opacity(0.4)
    mark.fill(gene_color)
    if $SHADE_HITS == 'both' then mark.fill_opacity(0.25) else mark.fill_opacity(0.8) end
    if direction == 'right'
      if (end_x - start_x + 1) < (arrow_width * 2) #if too short just draw triangle
        mark.polyline(start_x, contig.disp_y, end_x, contig.disp_y + (0.33 * contig.height).ceil, start_x, contig.disp_y + (0.66 * contig.height), start_x, contig.disp_y)
      else
        mark.polyline(start_x, contig.disp_y, (end_x - arrow_width), contig.disp_y, end_x, contig.disp_y + (0.33 * contig.height).ceil, (end_x - arrow_width), contig.disp_y + (0.66 * contig.height).ceil, start_x, contig.disp_y + (0.66 * contig.height).ceil, start_x, contig.disp_y)
      end
    elsif direction == 'left'
      if (end_x - start_x + 1) < (arrow_width * 2) #if too short just draw triangle
        mark.polyline(end_x, contig.disp_y, start_x, contig.disp_y + (0.33 * contig.height).ceil, end_x, contig.disp_y + (0.66 * contig.height).ceil, end_x, contig.disp_y)
      else
        mark.polyline(end_x, contig.disp_y, (start_x + arrow_width), contig.disp_y, start_x, contig.disp_y + (0.33 * contig.height).ceil, (start_x + arrow_width), contig.disp_y + (0.66 * contig.height).ceil, end_x, contig.disp_y + (0.66 * contig.height).ceil, end_x, contig.disp_y)
      end      
    else #square
      mark.polyline(end_x, contig.disp_y.ceil, start_x, contig.disp_y.ceil, start_x, contig.disp_y + (0.66 * contig.height).ceil, end_x, contig.disp_y + (0.66 * contig.height).ceil, end_x, contig.disp_y.ceil)    
    end

  elsif location == 'full' && gcount % 2 == 1 && $ALTERNATE_LABELS == true #draw style full with text on top
    
    mark.text_align(CenterAlign) 
    txt_center = start_x + ((end_x - start_x + 1) * 0.5)
    mark.font_style(ItalicStyle)
    mark.fill('white')
    txt_width = mark.get_type_metrics(gene_name.upcase).width.ceil

    #Draw white box around text if needed
    if $GENE_BOX == true
      mark.fill_opacity(0.8)
      mark.polyline(
        txt_center - 2 -  (0.5 * txt_width),(contig.disp_y + 1),
        txt_center + 2 + (0.5 * txt_width), (contig.disp_y + 1),
        txt_center + 2 + (0.5 * txt_width), (contig.disp_y + (0.33 * contig.height).floor - 1),
        txt_center - 2 - (0.5 * txt_width), (contig.disp_y + (0.33 * contig.height).floor - 1),
        txt_center - 2 - (0.5 * txt_width), (contig.disp_y + 1)
      )
    end
      
    #Draw gene name
    if($GNAME_TEXT_REMOVE == false)
      mark.fill_opacity(1)
      mark.fill(gene_color)
      mark.text(txt_center + 1, contig.disp_y + (0.3 * contig.height), gene_name.upcase) if draw_gene_name_color == true #give gene color highlighting 
      mark.fill('black')
      mark.text(txt_center, contig.disp_y + (0.3 * contig.height), gene_name.upcase)
    else
      puts "Skipping name/text for: #{gene_name.upcase}." if $DEBUG == true
    end

    #Draw only BLAST hit segment
    if $SHADE_HITS == 'both'
      mark.stroke('black')
      mark.stroke_opacity(0.4)
      mark.fill(gene_color)
      mark.fill_opacity(0.5)
      if direction == 'right'
        if (end_segment_x - start_segment_x + 1) < (arrow_width * 2) #if too short just draw triangle
          mark.polyline(start_segment_x, contig.disp_y + (0.33 * contig.height).floor, end_segment_x, contig.disp_y + (0.66 * contig.height).floor, start_segment_x, (contig.disp_y + contig.height).floor, start_segment_x, contig.disp_y + (0.33 * contig.height).floor)
        else
          mark.polyline(start_segment_x, contig.disp_y + (0.33 * contig.height).floor, (end_segment_x - arrow_width), contig.disp_y + (0.33 * contig.height).floor, end_segment_x, (contig.disp_y + (0.66 * contig.height)).floor, (end_segment_x - arrow_width), (contig.disp_y + contig.height).floor, start_segment_x, (contig.disp_y + contig.height).floor, start_segment_x, contig.disp_y + (0.33 * contig.height).floor)
        end
      elsif direction == 'left'
        if (end_segment_x - start_segment_x + 1) < (arrow_width * 2) #if too short just draw triangle
          mark.polyline(end_segment_x, contig.disp_y + (0.33 * contig.height).floor, start_segment_x, contig.disp_y + (0.66 * contig.height).ceil, end_segment_x, (contig.disp_y + contig.height).floor, end_segment_x, contig.disp_y + (0.33 * contig.height).floor)
        else
          mark.polyline(end_segment_x, contig.disp_y + (0.33 * contig.height).floor, (start_segment_x + arrow_width), contig.disp_y + (0.33 * contig.height).floor, start_segment_x, contig.disp_y + (0.66 * contig.height).floor, (start_segment_x + arrow_width), (contig.disp_y + contig.height).floor, end_segment_x, (contig.disp_y + contig.height).floor, end_segment_x, contig.disp_y + (0.33 * contig.height).floor)
        end
      else #square
        mark.polyline(end_segment_x, contig.disp_y + (0.33 * contig.height).floor, start_segment_x, contig.disp_y + (0.33 * contig.height).floor, start_segment_x, (contig.disp_y + contig.height).floor, end_segment_x, (contig.disp_y + contig.height).floor, end_segment_x, contig.disp_y + (0.33 * contig.height).floor)
      end
      mark.fill_opacity(0.25)
    end

    #Draw main full-size gene box
    mark.stroke('black')
    mark.stroke_opacity(0.4)

    mark.fill(gene_color)
    if $SHADE_HITS == 'both' then mark.fill_opacity(0.25) else mark.fill_opacity(0.8) end
    if direction == 'right'
      if (end_x - start_x + 1) < (arrow_width * 2) #if too short just draw triangle
        mark.polyline(start_x, contig.disp_y + (0.33 * contig.height).ceil, end_x, contig.disp_y + (0.66 * contig.height).ceil, start_x, (contig.disp_y + contig.height).floor, start_x, contig.disp_y + (0.33 * contig.height).ceil)
      else
        mark.polyline(start_x, contig.disp_y + (0.33 * contig.height).ceil, (end_x - arrow_width), contig.disp_y + (0.33 * contig.height).ceil, end_x, (contig.disp_y + (0.66 * contig.height)).ceil, (end_x - arrow_width), (contig.disp_y + contig.height).floor, start_x, (contig.disp_y + contig.height).floor, start_x, contig.disp_y + (0.33 * contig.height).ceil)
      end
    elsif direction == 'left'
      if (end_x - start_x + 1) < (arrow_width * 2) #if too short just draw triangle
        mark.polyline(end_x, contig.disp_y + (0.33 * contig.height).ceil, start_x, contig.disp_y + (0.66 * contig.height).ceil, end_x, (contig.disp_y + contig.height).floor, end_x, contig.disp_y + (0.33 * contig.height).ceil)
      else
        mark.polyline(end_x, contig.disp_y + (0.33 * contig.height).ceil, (start_x + arrow_width), contig.disp_y + (0.33 * contig.height).ceil, start_x, contig.disp_y + (0.66 * contig.height).ceil, (start_x + arrow_width), (contig.disp_y + contig.height).floor, end_x, (contig.disp_y + contig.height).floor, end_x, contig.disp_y + (0.33 * contig.height).ceil)
      end
    else #square
        mark.polyline(end_x, contig.disp_y + (0.33 * contig.height).ceil, start_x, contig.disp_y + (0.33 * contig.height).ceil, start_x, (contig.disp_y + contig.height).floor, end_x, (contig.disp_y + contig.height).floor, end_x, contig.disp_y + (0.33 * contig.height).ceil)	    
    end
    
  else
    raise "ERROR: from draw_gene: Invalid location #{location} provided for gene #{gene_name} on contig #{contig.name}!"
  end
  
  return mark
end

#Helper-Function for draw_connections - requires 'mark' in progress from that function.
#On Entry: <draw> mark (from draw_connections), <array> line (array of coord info), <array> all_midpoints (pre-calc'd midpoints for arrows locs), <bool> dropshadow, linetype = straight/curved
#line array contains at index 0-6 accordingly: [line_bot_x, line_bot_y, line_top_x, line_top_y, line_color, flip_direction,gname]
#On Exit: Mark with arrow added is returned, if $last_x was 1 set to -1 and vice-versa to adjust arrow movement up/down
def draw_arrow(mark, line, contig_height, all_midpoints, level_markers, dropshadow = true, linetype = 'straight')

  midpt_x = (line[0] + line[2]) / 2
  midpt_y = (line[1] + line[3]) / 2
  org_y = midpt_y
  org_x = midpt_x  
  arrow_slope = 1.0
  line_slope = (line[3] - line[1]) / (line[2] - line[0])
  pass_check = false
  max_try = 5 # number of times to try to adjust arrow before giving up
  if $last_x == 1 then $last_x = -1 else $last_x = 1 end #switch adjust direction
  arrow_direction = $last_x  
  pad = 1 #dropshadow offset
  offset = 11.5 #increase or decrease size of arrow
  detect_space = $ARROW_SPACE # px required between arrows and used to move them if clash detected
  mark.stroke_width(1)
  
  #Get array direction and set parameters - these are manually coded so the arrows look correct
  if line[5] == 'flipleft'
    arc_start = 55
    arc_end =  300
    arrow_offset = 48
    rad_adj = 1.0
    dist_offset = -0.9
  elsif line[5] == 'flipright'
    arc_start = 245
    arc_end = 120
    arrow_offset = 316 #increase to move arrow head right, lower to go left
    rad_adj = 1.0
    dist_offset = 0.9 #width of arrow head
  else
    raise "ERROR from draw_arrow: invalid flip direction from line coord array provided! #{line[5]}"
  end
  
  #Gather location info for drawing arcs
  while(pass_check == false && max_try > 0)
    pass_check = true
    #Check if arrow could be behind contig
    level_markers.each do |m|
      if m < midpt_y && (m + contig_height) > midpt_y
        puts "WARNING: arrow could appear behind contig... adjust height!" if $DEBUG == true
        #Add adjustment
        old_y = midpt_y
        old_x = midpt_x
        midpt_y -= (contig_height * 1.8)
        midpt_x = ((midpt_y - old_y) / line_slope) + old_x
        puts "Re-Adjusting Drawing Arrow from #{old_x}x, #{old_y}y to #{midpt_x}x, #{midpt_y}y" if $DEBUG == true
        pass_check = false
      end
    end
    #Check if arrows overlap
    all_midpoints.each do |m|
      if line[6] != m[2]
        this_mx = m[0]
        this_my = m[1]
        old_y = midpt_y
        old_x = midpt_x
        if( ((midpt_x - this_mx).abs < detect_space) && (((midpt_y - this_my).abs < detect_space )))
          midpt_y += (detect_space * arrow_direction)
          midpt_x = ((midpt_y - old_y) / line_slope) + old_x
          puts "Re-Adjusting Arrow for #{line[6]} from #{old_x}, #{old_y} to #{midpt_x}, #{midpt_y} due to conflict with #{m[2]}" if $DEBUG == true
          pass_check = false
          break
        end
      end
    end
    #Update w/ changes
    all_midpoints.each do |m|
      if m[2] == line[6] && org_x == m[0] && org_y == m[1]
        m[0] = midpt_x
        m[1] = midpt_y
      end
    end
    max_try -= 1
  end
  puts "Gave up retrying arrows for #{line[6]}!" if max_try == 0 && $DEBUG == true
  puts "Drawing Arrow: #{midpt_x}, #{midpt_y}" if $DEBUG == true

  #Gather location info for drawing arrow head
  top_x = (offset / arrow_slope) + midpt_x 
  top_y = arrow_slope * (top_x - midpt_x) + midpt_y
  bot_x = ((0 - offset) / arrow_slope) + midpt_x
  bot_y = arrow_slope * (bot_x - midpt_x) + midpt_y
  radius = Math.sqrt( (midpt_x - top_x)**2 + (midpt_y - top_y)**2) * rad_adj

  #Draw arcs and arrow
  x1 = midpt_x + (radius * Math.sin(arrow_offset * 0.0174532925))
  y1 = midpt_y + (radius * Math.cos(arrow_offset * 0.0174532925))
  x2 = x1 + (radius * dist_offset * 0.5)
  y2 = y1 - (radius * 0.3)
  dist = Math.sqrt((x2 - x1)**2 + (y2 - y1)**2)
  x3 = x2
  y3 = y1 + (radius * 0.3)
  
  #add shadows?
  if dropshadow == true
    mark.stroke('gray')
    mark.stroke_opacity(0.2)
    mark.fill_opacity(0.2)
    mark.fill('none')
    mark.arc(top_x + 3, top_y + 3, bot_x + 3, bot_y + 3, arc_start, arc_end)
    mark.fill('gray')
    mark.stroke('none')
    mark.polyline(x1+pad,y1+pad, x2+pad,y2+pad, x3+pad,y3+pad, x1+pad,y1+pad)
  end
  
  #colored arcs
  mark.stroke('black')
  mark.stroke_opacity(0.8)
  mark.fill('none')
  mark.arc(top_x + 2, top_y + 2, bot_x + 2, bot_y + 2, arc_start, arc_end)
  mark.stroke(line[4]) #get line color
  mark.arc(top_x + 1, top_y + 1, bot_x + 1, bot_y + 1, arc_start, arc_end)
  mark.arc(top_x, top_y, bot_x, bot_y, arc_start, arc_end)
  mark.stroke('black')
  mark.arc(top_x - 1, top_y -1, bot_x -1, bot_y -1, arc_start, arc_end)

  #arrow
  mark.stroke('black')
  mark.stroke_opacity(0.8)
  mark.fill(line[4])
  mark.polyline(x1,y1 , x2,y2, x3,y3, x1,y1)
  
  #Restore covered sections of connecting line that were covered by arrow IF type is straight and not dashed
  #makes it look like arrow is going around line- not currently implemented for curved lines
  if linetype == 'straight' && ((line[3] - line[1]) <= (level_markers[1] - level_markers[0]))
    #shadow
    mark.stroke_dasharray()
    mark.stroke_opacity(1)
    mark.fill('gray')
    mark.stroke_width(1)
    mark.line(line[0]+1,line[1] + 1,midpt_x+1,midpt_y + 1) if dropshadow == true
    #colored line
    mark.stroke_opacity(1)
    mark.stroke_width(1)
    mark.stroke(line[4])
    mark.line(line[0],line[1],midpt_x,midpt_y)
  end

  return all_midpoints
end

#draw_connections - either draws or gets information on connecting lines between matching genes
#types = 'topdown' (only between genome and the ones below it... cleaner look) or 'all' (all genomes to all genomes... can get very busy looking)
#modes = 'draw' (actually draw the connections, 'getlength' (don't draw but return the cumulative Euclidean distance (in pixels) that the lines make for optimizations)
#line_type can be 'straight' or 'curved'
#Reminder of passed in gdata array structure (should switch this to struct when there's time for better readability): [name, x_start + ( 0.5 * width), y_top, x_start + ( 0.5 * width) , y_bottom, total_x_width, direction]
#On exit returns mark
def draw_connections(gdata, gcolors, level_markers, contig_height, img_height, img_width, type='topdown', mode='draw', dropshadow=true, linetype='curved')

  #Collect line coords
  line_coords = Array.new# array container for coords to draw lines: [top_x, top_y, bottom_x, bottom_y]
  all_midpoints = Array.new# array container for midpoint coords [midpt_x, midpoint_y, gname]
  curve_amount = 15 # for curved lines
  pad = 1 #dropshadow offset
  gnames = gdata.collect {|g| g[0]}
  gnames.each do |n|
    #puts "Connecting Gene: #{n} with connection type #{type}"
    this_gene = gdata.find_all {|g| g[0] == n }
    this_gene.sort! {|a,b| a[2] <=> b[2]}

    if type == 'topdown'
      while this_gene.length > 0
        g1 = this_gene.shift
        tmp_coords = []
        if this_gene.length >= 1
          this_gene.each do |g2|
            if( (g1[6] == g2[6]) || g1[6] == 'square' || g2[6] == 'square' )
              dir = 'noflip'
            elsif g1[6] == 'left' && g2[6] == 'right'
              dir = 'flipleft'
            elsif g1[6] == 'right' && g2[6] == 'left'
              dir = 'flipright'
            else
              raise "ERROR in draw_connections! Invalid gene direction given: #{gdata}"
            end
            #puts "Line Coords: [#{g1[3]},#{g1[4]},#{g2[1]},#{g2[2]},#{gcolors[n]},#{dir},#{n}]"
            tmp_coords << [g1[3],g1[4],g2[1],g2[2],gcolors[n],dir,n] if (g2[4] - g1[4]) > contig_height # only draw if g2's y_top is < g1's y_bottom
          end
          min_y = tmp_coords.collect { |a| a[3] }.min 
          line_coords += tmp_coords.find_all {|g| (g[3] >= min_y) && g[3] < (min_y + contig_height) }
        end
      end
    elsif type == 'all'
      while this_gene.length > 0
        g1 = this_gene.shift
        this_gene.each do |g2| 
          if( (g1[6] == g2[6]) || g1[6] == 'square' || g2[6] == 'square' )
            dir = 'noflip'
          elsif g1[6] == 'left' && g2[6] == 'right'
            dir = 'flipleft'
          elsif g1[6] == 'right' && g2[6] == 'left'
            dir = 'flipright'
          else
            raise "ERROR in draw_connections! Invalid gene directions given: G1: #{g1[6]} - G2: #{g2[6]}}"
    end
          line_coords << [g1[3],g1[4],g2[1],g2[2],gcolors[n],dir,n] if (g2[4] - g1[4]) > contig_height
        end
      end
    else
      raise "ERROR! Invalid connection type #{type} given. Must be all 'topdown' or 'all.'"
    end
  end
  line_coords.uniq! #remove duplicates
  
  #Draw lines
  if mode == 'draw'
    mark = Magick::Draw.new
    mark.stroke_antialias(true)
    mark.stroke_width(1)
    
    #Save all midpoints for moving arrows if line overlaps are very numerous for draw_arrow function 
    line_coords.each {|line| all_midpoints << [((line[0] + line[2]) / 2) , ((line[1] + line[3]) / 2), line[6]] if line[5] != 'noflip' }
    
    line_coords.each do |line|
      dash_line = false
      if level_markers.length > 1 && ((line[3] - line[1]) > (level_markers[1] - level_markers[0]))
        dash_line = true
      end

      #dropshadow lines
      if dropshadow == true
        mark.stroke('gray')
        #mark.stroke_opacity(1)
        mark.fill('gray')
        #mark.stroke_width(1)

        if dash_line == true
          mark.stroke_dasharray(15,10,5,10)
          mark.stroke_opacity(0.1)
        else
          mark.stroke_dasharray()
          mark.stroke_opacity(1)
        end

        if linetype == 'straight'
          mark.line(line[0] + pad,line[1] + pad, line[2] + pad,line[3] + pad)
        elsif linetype == 'curved' 
          mark.fill('none')
          #See Path API entry for Usage: http://www.imagemagick.org/RMagick/doc/draw.html#path
          mark.path("M#{line[0] + pad},#{line[1] + pad} Q#{line[0] + pad},#{line[1] + curve_amount + pad} #{(line[0] + pad + line[2] + pad) / 2},#{(line[1] + pad + line[3] + pad) / 2} T#{line[2] + pad},#{line[3] + pad}")
        else
          raise "ERROR in draw_connections! Invalid linetype '#{linetype}' selected. MUST be either 'straight' or 'curved'."
        end
      end
      
      #Main strokes
      mark.stroke(line[4])
      #mark.stroke_opacity(1)
      #mark.stroke_width(1)
      
      if dash_line == true #if line passes through adjacent genome 
        mark.stroke_dasharray(15,10,5,10) #15px line, 10px space, 5px line 10px space
        mark.stroke_opacity(0.5)
      else
        mark.stroke_dasharray()
        mark.stroke_opacity(1)
      end

      if linetype == 'straight'
        mark.line(line[0],line[1],line[2],line[3])
      elsif linetype == 'curved' 
        mark.fill('none')
        # See Path API entry for Usage: http://www.imagemagick.org/RMagick/doc/draw.html#path
        mark.path("M#{line[0]},#{line[1]} Q#{line[0]},#{line[1] + curve_amount} #{(line[0] + line[2]) / 2},#{(line[1] + line[3]) / 2} T#{line[2]},#{line[3]}")
      else
        raise "ERROR in draw_connections! Invalid linetype '#{linetype}' selected. MUST be either 'straight' or 'curved'."
      end
    end

    #Draw arrows last so they're on top
    mark.stroke_dasharray()
    line_coords.each {|line| if line[5]  != 'noflip' then all_midpoints = draw_arrow(mark, line, contig_height, all_midpoints, level_markers, dropshadow, linetype) end }
    
    return mark

  elsif mode == 'getlength'
    total_length = 0.0 
    line_coords.each {|line| total_length += Math.sqrt((line[2] - line[0])**2 + (line[3] - line[1])**2) }
    puts "LINE COORD LEN: #{total_length}" if $DEBUG == true
    return total_length
  else
    raise "ERROR in draw_connections: Invalid value '#{mode}' given for mode. Must be 'draw' or 'getlength'!"
  end
end

#read_cmap(file) - Get raw annotation info
#On Entry: file is a single CMAP file for a single genome is given following this convention:
#
#ContigName1 {1  [ Contig#GeneStart (Gene#Start Gene_Name ==>  Gene#End) Contig#GeneEnd ]  [ ... more genes go here]  FinalContig#}
#...
#ContigNameN {1  [ Contig#GeneStart (Gene#Start <== Gene_Name  Gene#End) Contig#GeneEnd ]  [ ... more genes go here]  FinalContig#}
#
#On Exit: total_bp across all contigs <int>, contig end annotations <array>, blocks of start/end bp [bp_start, bp_end] <array>, and strings of raw contig data <array> are returned.
def read_cmap(file)
  
  lpad = 1000 #padding to add to front of contig for naming
  rpad = 1000 #padding to add to end of contig
  maxgenes = 0 #max number of genes found within this genome
  contigs = Array.new
  end_annotations = Array.new
  end_bp = Array.new
  total_bp, gstart, gend = 0,0,0
  data = File.readlines(file)
  begin
    puts "Reading in CMAP file: #{file}." if $DEBUG == true
  rescue
    raise "Error opening file: #{file}!\n Please ensure file and CMAP directory exist!"
  end
  
  data.each do |line|
    maxgenes += line.count('(') #Add number of genes to this contig
    
    if line =~ /([A-Z0-9\_\(\)\-\+\>\<\.\!]+)(\s+\{)(\d+)(\s\s\[\s)(\d+)(.*?)(\d+)(\s\]\s\s)(\d+)(\})/i
      contigs << "#{$1}|#{$5}#{$6}#{$7}"
      cstart = $3.to_i
      cend = $9.to_i
      gstart = $5.to_i
      gend = $7.to_i

      #Check for annotating ends of contigs with hash marks if incomplete
      if gstart > lpad
        gstart -= lpad
        draw_ends = 'left' #note left side won't start at 1
      else
        gstart = 1
        draw_ends = 'all'
      end
        
      if gend < (cend - rpad)
        gend += rpad
        if draw_ends == 'left' then draw_ends = 'none' else draw_ends = 'right' end #adjust for right side drawing
      else
        gend = cend
      end
        
      total_bp += (gend - gstart + 1)
      end_annotations << draw_ends
      end_bp << [gstart, gend]

    elsif line =~ /[\w\d]/i #raise error unless it's just a blank lines at end of cmap file
      raise "ERROR! In file #{file} invalid contig line encountered!\n#{line}"
    end
  end

  return maxgenes, total_bp, end_annotations, end_bp, contigs
end

#Converts x in an old range of numbers to a new one
def convert_range(x, oldmax, oldmin, newmax, newmin)
  old_diff = (oldmax - oldmin)  
  new_diff = (newmax - newmin)
  return ( (((x - oldmin) * new_diff) / old_diff.to_f ) + newmin).ceil
end

#Optimize layout of levels to minimize total line length. Keeps top genome in place.
# WARNING! Function is greedy and not meant to scale to huge numbers of genomes as it uses permutations of genome arrangements... will slow down quickly with each new genome added!
#On Entry: contig_layout, contig_structs, gene_structs and gene names given as well as opt_type which is either 'dist' for minimizing euclidian distance else 'flip' which minimized gene flips
#On Exit returns optimized contig_layout as array
def optimize_layout(contig_layout, contig_structs, gene_structs, opt_type='dist')

  if opt_type == 'dist'
    puts "Optimizing contig layout to reduce total connection distance." if $DEBUG == true
  else
    puts "Optimizing contig layout to reduce total number of flips." if $DEBUG == true
  end

  level_range = (0 ... contig_layout.length).to_a
  all_ranges = level_range.permutation.to_a
  best_range = all_ranges[0] # for Euclid. dist. optimization
  overall_best_length = 999999999.9
  overall_least_flips = 999999999
  
  #Get initial default coords
  gene_coords = Array.new # container of coord blocks for gene markers [name, top-x,top-y, bottom-x,bottom-y,dir]  
  best_range.each do |i|

    cgenes = gene_structs.find_all {|g| g.genome_number == i}
    cgenes.each do |g| 
      c = contig_structs.find_all { |h| h.name == g.contig && h.genome_number == g.genome_number }[0]
      gx_start = convert_range(x=g.bp_start, c.bp_end, c.bp_start,(c.disp_x + c.width), c.disp_x)
      gx_end = convert_range(x=g.bp_end, c.bp_end, c.bp_start,(c.disp_x + c.width), c.disp_x)
      gx_width = gx_end - gx_start + 1
      gene_coords << [g.name, gx_start + (0.5 * gx_width), 0, i, g.dir] # [name, x_midpoint, y_placeholder, level, dir]
    end
  end
  
  #Try each combination
  all_ranges.each do |level_range|
    this_length = 0
    these_flips = 0

    #Set Y values for this range
    these_coords = Array.new
    this_y = 0
    level_range.each do |j|
      gene_coords.find_all { |g| g[3] == j }.each {|h| h[2] = this_y; these_coords << [h[0],h[1],h[2],h[3],h[4]] }
      this_y += 10
    end

    #get dist for each gene name
    gnames = these_coords.collect {|g| g[0]}.uniq!
    gnames.each do |n|
      this_gene = these_coords.find_all {|g| g[0] == n }
      this_gene.sort! {|a,b| a[2] <=> b[2]}
      
      while this_gene.length > 0
        g1 = this_gene.shift
        if this_gene.length >= 1
          g2 = this_gene[0]
          if g1[4] == 'left' && g2[4] == 'right'
            these_flips += 1
          elsif g1[4] == 'right' && g2[4] == 'left'
            these_flips += 1
          end
          this_length += Math.sqrt((g2[1] - g1[1])**2 + (g2[2] - g1[2])**2) if g2[2] > g1[2]
          next if this_length > overall_best_length && opt_type == 'dist'
          next if these_flips > overall_least_flips && opt_type != 'dist'
        end
      end
    end
    
    #update best scores
    if opt_type == 'dist' && this_length < overall_best_length
      best_range = level_range
      overall_best_length = this_length
      overall_least_flips = these_flips
      puts "New current best distance order: #{best_range} - Best Length: #{overall_best_length}" if $DEBUG == true

    elsif opt_type != 'dist' && these_flips < overall_least_flips
      overall_best_length = this_length
      overall_least_flips = these_flips
      best_range = level_range
      puts "New current best distance order: #{best_range} - Least Flips: #{overall_least_flips}" if $DEBUG == true

    #break ties using distance if judging number of flips
    elsif opt_type != 'dist' && these_flips == overall_least_flips && this_length < overall_best_length
      overall_best_length = this_length
      overall_least_flips = these_flips
      best_range = level_range
      puts "New current best distance order: #{best_range} - Least Flips: #{overall_least_flips} - Tie broken with best length: #{overall_best_length}" if $DEBUG == true
    end
  end

  puts "Reordered genome indexes as: [#{best_range.join(',')}]. Connection length: #{overall_best_length} Number of flips: #{overall_least_flips}" if $DEBUG == true
  return best_range
end

#get_minmax_gene_bases
#On entry: Formatted contig_layout array is provided
#On exit: min_gene_size, max_gene_size, min_genome_size, max_genome_sizes are returned in terms of basepairs
def get_minmax_gene_bases(contig_layout)
  
  #Pre-calculated the largest gene size
  max_gene_bp = 0
  max_gene_idx = 0
  genome_sizes = []
  gene_sizes = []
  gene_counts = []
  contig_layout.each_index do |i|
    genome_size = 0
    gene_count = 0
    contig_layout[i].each_index do |c|
      if contig_layout[i][c] =~ /(\|)(.*)/i
        gene_string = $2
        gc = gene_string.count('(')
        gene_count += gc
        if gc > 1 then genes = gene_string.split("]  [") else genes = [gene_string] end
        genes.each do |g|
          if g =~ /(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)(\s.*?\s)(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)/
            gstart = $1.to_i
            gend = $15.to_i
            
            gdiff = gend - gstart + 1
            if gdiff > max_gene_bp
              max_gene_bp = gdiff
              max_gene_idx = i
            end
            gene_sizes << gdiff
            genome_size += gdiff
          else
            raise "ERROR in get_minmax_gene_bases: Invalid contig string encountered: #{contig_layout[i][c]}"
          end
        end
      end  
    end
    gene_counts << gene_count    
    genome_sizes << genome_size
  end
  
  puts "Max Gene Size: #{gene_sizes.max}bp (genome: #{max_gene_idx}), Min Gene Size: #{gene_sizes.min}, Max Genome Size: #{genome_sizes.max}bp, Min Genome size: #{genome_sizes.min}bp." if $DEBUG == true

  return gene_sizes, genome_sizes, gene_counts, max_gene_idx
end


#==================================================
# MAIN PROGRAM
# Step1: Set variables
# Step2: Read in CMAP data and get coordinates - Note CMAP should be pre-validated we don't do strict checks.
# Step3: Assign gene colors
# Step4: Generate then save image

#*****************************************************
#Step1: VARIABLE DECLARATIONS
img_width = $IMG_WIDTH
img_height = $IMG_HEIGHT
contig_height = $CONTIG_HEIGHT# 50
contig_start_x = img_width * $GNAME_BUFFER_PERCENT #buffer of space to left (for writing genome name) before starting contigs
contig_start_y = img_height * 0.15 #buffer of space on top/bottom before starting contigs
contig_width_percent = 0.95 # percent as decimal of width contigs should take up of screen
gene_space_width_percent = 0.55 #percent of width devoted to showing gene blocks for genes display mode
arrow_width = 20 # width of arrow drawn on gene boxes- NOT flip-arrows
genome_name_pt_size = $TEXT_SIZE #32
draw_dropshadow = true
optimized_contig_order = false

puts "SimpleSyntenyDrawer-Version #{$VERSION}" if $DEBUG == true
puts "Starting to generate image for syntany map with WIDTH=#{img_width}px and HEIGHT=#{img_height}px." if $DEBUG == true


#*****************************************************
#Step2: DATA INPUT , COORDINATE PROCESSING, ORDER OPTIMIZATION

#Read in CMAP info...
puts "Checking for CMAP Files in DIR: #{$CMAP_DIR}"
Dir.chdir($CMAP_DIR)
initial_genome_names = Array.new
cmap_files = Dir.glob("*.cmap", File::FNM_CASEFOLD).sort
cmap_files.each {|f| if f =~ /(\d*)(.*)(\.cmap)/i then puts "Found: #{$2}#{$3}"; initial_genome_names << $2.gsub(/\+/,' ') end}
if initial_genome_names.length == 0 then puts "\nERROR: No files with a .cmap extension were found in the provided location: #{$CMAP_DIR}\n\nUse 'SyntenyDrawer -help' to see details on using the program. Exiting now!\n\n" ; exit end
	

#Test for calculated width of longest genome name for given font family and size
tempmark = Magick::Draw.new
tempmark.font_style = ItalicStyle
tempmark.pointsize = genome_name_pt_size
tempmark.font_family = "Arial"
tempmark.font_weight = BoldWeight
max_gname_width = tempmark.get_type_metrics(initial_genome_names.max_by(&:length)).width.ceil
puts "Longest genome name width calculated as: #{max_gname_width}px. Offsetting contig_start_x now." if $DEBUG == true
contig_start_x += max_gname_width

cmap_order = (0 ... cmap_files.length).to_a #order that files are read... we start from 0 ... n-1 CMAPs unless later optimized

#Gather initial contig stats, return here if a reorder is needed based on optimization
while(optimized_contig_order == false)
  
  max_bp = 0 #genome level with the max num of cumulative bp's across all contigs
  max_genes = 0
  min_genes = 99999999
  total_genes = 0

  #Array to hold other arrays for raw data on each genome (level) e.g.
  #  genome1  [[contig1,contig2,contig3]],
  #  genome2  [[contig1,contig2],
  #  genome3  [[contig1,contig2,contig3,contig4]]
  contig_layout = Array.new

  genome_total_bp = Array.new #holds max bp for each genome
  genome_end_bps = Array.new #holds array of start bp for each genome
  genome_ngenes = Array.new #holds array of # genes per genome
  genome_annotations = Array.new #holds strings for how to draw/annotate ends of contigs
  contig_structs = Array.new #Array of contig structs
  final_contig_structs = Array.new #Array of final contig struct ordering for optimization
  gene_structs = Array.new #Array of gene structs 
  genome_names = Array.new #Array of final genome names
  
  cmap_order.each do |cmap_idx|
    genome_names << initial_genome_names.at(cmap_idx)
    file = cmap_files.at(cmap_idx)
    ngenes, totalbp, end_anots, ebp, c = read_cmap(file) #returns [<array> #genes in this genome] [<int> total# base pairs), <string> end annotation, <block> [bpstart, bpend], <array> raw contig data]
    contig_layout << c
    genome_total_bp << totalbp
    genome_annotations << end_anots
    genome_end_bps << ebp
    genome_ngenes << ngenes
    max_genes = ngenes if ngenes > max_genes
    min_genes = ngenes if ngenes < min_genes
    total_genes += ngenes  
    max_bp = totalbp if totalbp > max_bp #total contig bp's
  end
  raise "ERROR: No valid CMAP files found at: #{$CMAP_DIR}" if contig_layout.length == 0

  #Adjust contig heights if too many genomes to fit on screen
  if (contig_layout.length * contig_height + 1) > img_height then
    contig_height = img_height / (contig_layout.length * contig_height + 1)
    contig_start_y = img_height
    puts "WARNING! Too many genomes to fit contigs at current settings. Adjusting contig heights to #{contig_height}!"
  end

  level_y_offset = contig_start_y
  level_markers = Array.new #mark level Y values to check for conflicts
  level_height_buffer = ((img_height - (1 * contig_start_y)) - (contig_layout.length * (contig_height - 40))) / contig_layout.length #y space to add between levels
  puts "Display area buffered to be X = [0, #{contig_start_x}]." if $DEBUG == true

  if $DISPLAY_MODE == 'contigs'
    raise "CONTIGS DISPLAY MODE HAS BEEN DEPRICATED!"
    #Code removed by Dan, 11.2.2014

  elsif $DISPLAY_MODE == 'genes'
    
    puts "Gathering initial contig and gene information for GENES DISPLAY MODE." if $DEBUG == true
    
    #contig_layout.each_index {|c| this_count = c.count('['); if this_count > max_num_genes then max_num_genes = this_count end}
    
    img_available = ((img_width * 0.99) - contig_start_x) #give a 1% width buffer on right side

    #Pre-calculate maximum gene width size
    gene_sizes, genome_sizes, gene_counts, max_gene_genome_index = get_minmax_gene_bases(contig_layout) #get longest and shortest gene length and array of total bp's of all genes
    scale_factor =  (img_available * gene_space_width_percent) / genome_sizes.max # converts bp to px
    space_bp = (img_available * (1 - gene_space_width_percent) * genome_sizes.max) / (img_available * gene_space_width_percent)
    #puts "Scale factor: #{scale_factor}# - MaxGeneSize: #{gene_size_max} - MaxGeneCount: #{gene_count_max}"
    
    contig_layout.each_index do |i|
      
      gene_spacing_buffer = (space_bp + genome_sizes.max - genome_sizes[i])  / (gene_counts[i]) 

      total_width = 0.0
      level_x_offset = contig_start_x 
      these_contig_structs = []
      ncontigs = 0
      
      contig_layout[i].each_index do |c|
        this_contig = Contig.new
        cgene_count = contig_layout[i][c].count("(") #gene count for this contig
        if contig_layout[i][c] =~ /([A-Z\.\_0-9]+)(\|)(.*)/i
          
          cname = $1
          cstart = genome_end_bps[i][c][0]
          cend = genome_end_bps[i][c][1]
          previous_gend_bp = -1
          ccount = 1
          cgcount = 0
          
          #NOTE! Because :width and :disp_x start are not needed yet we temporarily store the original cstart and cend values in their places for now and update these if contigs are split!
          this_contig = Contig.new(i, "#{cname}.#{ccount}", 0, 0, contig_height, cstart, cend, level_y_offset, 'bisque', genome_annotations[i][c], 0, cend)

          if cgene_count > 1 then genes = $3.split("]  [") else genes = [$3] end
          genes.each do |g|
            gstart, gend = 0,0 #gene segment from blast hit
            gfullstart, gfullend = 0,0 #full gene size 
            gname, gdir = '',''
            cgcount += 1
            
            if g =~ /(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)(.*?)(==>)(.*?)(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)/
              gname = $8.strip
              gfullstart = $1.to_i
              gfullend = $17.to_i
              gstart = $3.to_i
              gend =  $15.to_i
              gdir = 'right'
            elsif g =~ /(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)(.*?)(<==\s)(.*?)(\s+)(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)/
              gname = $10.strip
              gfullstart = $1.to_i
              gfullend = $18.to_i
              gstart = $3.to_i
              gend =  $16.to_i
              gdir = 'left'
            elsif g =~ /(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)(.*?)(==\s)(.*?)(\s==)(\s\s)(\d+)(\|)(\d+)(.*?)(\d+)(\|)(\d+)/
              gname = $10.strip
              gfullstart = $1.to_i
              gfullend = $19.to_i
              gstart = $3.to_i
              gend =  $17.to_i
              gdir = 'square'   
            else
              raise "ERROR! Invalid gene (##{g}) in contig #{c} encountered! - #{genes[g]}"
            end  
                        
            #Is this gene too far away on the same contig? If so split the contig into two parts...
            if (gfullstart - previous_gend_bp + 1) > (gene_spacing_buffer)
              
              #ignore for very first contig otherwise save our contig in progress after some updates and start a new one
              if previous_gend_bp > 0
                this_contig.ngenes = cgcount
                total_width += this_contig.bp_end - this_contig.bp_start + 1 + (0.33 * gene_spacing_buffer)
                ncontigs += 1
		this_contig.disp_x = previous_gend_bp + (0.33 * gene_spacing_buffer)
                these_contig_structs << this_contig
                ccount += 1
                cgcount = 0
              end
              
              this_contig = Contig.new(i, "#{cname}.#{ccount}", gfullstart, gfullend, contig_height, cstart, cend, level_y_offset, 'bisque', genome_annotations[i][c], cgcount, cend)
              
            #Continue adding genes to the same contig...  
            else
              this_contig.bp_end = gfullend
              cgcount += 1
            end
            
            gene_structs << Gene.new(i, "#{cname}.#{ccount}", gname, gfullstart, gfullend, gstart, gend, gdir)
            previous_gend_bp = gfullend

            puts "Loaded gene: #{gname} on Genome: #{i} - #{cname}.#{ccount}" if $DEBUG == true
          end
        else
          raise "ERROR! Invalid string for genome (##{i}) in contig #{c} encountered! - #{contig_layout[i][c]}"
        end
        
        #Add final contig
        total_width += (this_contig.bp_end - this_contig.bp_start + 1)
        this_contig.ngenes = cgcount
        ncontigs += 1
        these_contig_structs << this_contig
      end
      
      #Extract contig spacing info
      puts "Number of contigs for genome #{i} after splitting: #{ncontigs}." if $DEBUG == true
      
      contig_space = (genome_sizes.max + space_bp - total_width) / (ncontigs) #space inbetween contigs
      contig_edge_bp = gene_spacing_buffer * 0.33 #number of bp padding on either side to add
      
      these_contig_structs.each do |this_c|
          
        #See above note these values were temporarily store and will be overwritten
        cstart = this_c.width.floor
        cend = this_c.disp_x.ceil
        
        this_c.bp_start = [(this_c.bp_start - contig_edge_bp).floor, cstart].max
        this_c.bp_end = [(this_c.bp_end + contig_edge_bp).ceil, cend].min
        this_c.width = (convert_range(x=((this_c.bp_end - this_c.bp_start + 1)), genome_sizes.max, 1, img_available * gene_space_width_percent, 1)).ceil

        #adjust end annotations
        partial_offset = 0
        if this_c.name !~ /.*\.1$/ #is this a continuation of a partial contig? 
          this_c.bp_complete = 'partial_left'
          partial_offset = 0.33
        elsif this_c.bp_start > cstart && this_c.bp_complete == 'all'
          this_c.bp_complete = 'left'
        elsif this_c.bp_start >  cstart && this_c.bp_complete == 'right'
          this_c.bp_complete = 'none'
        end
        
        if this_c.bp_end < this_c.full_contig_end && this_c.bp_complete == 'partial_left'
          this_c.bp_complete = 'partial_right'
        elsif this_c.bp_end < this_c.full_contig_end && this_c.bp_complete == 'left'
          this_c.bp_complete = 'none'
        elsif this_c.bp_end < this_c.full_contig_end && this_c.bp_complete == 'all'
          this_c.bp_complete = 'right'
        end
        
        this_c.disp_x = (level_x_offset - convert_range(x=(contig_space * partial_offset), genome_sizes.max, 1, (img_available * gene_space_width_percent), 1)).ceil #add space penalty if this is a partial contig
        level_x_offset += convert_range(x=(contig_space * 0.6) - (contig_space * partial_offset), genome_sizes.max, 1, (img_available * gene_space_width_percent), 1) + this_c.width
      end
      
      #Center and save contigs
      these_contig_structs.each { |this_c| this_c.disp_x += ((img_available - level_x_offset + contig_start_x) / 2.0); contig_structs << this_c }
      level_y_offset += level_height_buffer
      level_markers << level_y_offset
    end
      
  else
    raise "ERROR: Invalid Display Mode selected. Must be 'contigs' or 'genes'!"
  end

  #Optimize layout if desired - either 'dist' else 'flip'
  unless $CONTIG_OPTIMIZE == 'no'
    cmap_order = optimize_layout(contig_layout, contig_structs, gene_structs, $CONTIG_OPTIMIZE)
    $CONTIG_OPTIMIZE = 'no' # don't run a second time
  else
    final_contig_structs = contig_structs #genomes appear in alphabetical order of submitted CMAPs
    optimized_contig_order = true
  end
end 


#*****************************************************
#Step3: Assign Gene Colors

puts "Assigning colors to gene names." if $DEBUG == true

gene_names = gene_structs.collect {|g| g.name}.uniq
gene_colors = Hash.new # assigned colors for each gene (used by rest of program)
existing_colors = Hash.new # used to hold saved colors (if present)
colors = ['IndianRed1','CornflowerBlue','green','MediumOrchid1','DarkGray','yellow2','tan3','DarkOrange','DodgerBlue','SpringGreen2','YellowGreen','DarkGoldenrod3','salmon','khaki4','DarkOliveGreen1','aquamarine','SlateGray3','lavender','MediumPurple','plum1','HotPink3','firebrick2','MistyRose3','OrangeRed','peru','violet','snow4','firebrick4','RosyBrown4','SeaGreen3','MediumForestGreen','LightSeaGreen']

#Load color list long enough to handle all genes present in genes file
if colors.length < gene_names.length #add more random colors as needed
  diff = (gene_names.length - colors.length)
  colors += diff.times.map{"#%06x" % (rand * 0x1000000)}
end

#If there is a saved color list, load it and remove used colors from colors array. Format for each line in list is: "GENE_NAME::GENE_COLOR"
if File.exist?($COLOR_LIST)
  puts "Opening a saved color list." if $DEBUG == true

  File.readlines($COLOR_LIST).each do |sc|
    if sc =~ /^(.*)\:\:(.*)$/
      existing_colors[$1] = $2
      colors.delete($2) #remove from available colors list
    end
  end
else
  puts "No color list exists yet!" if $DEBUG == true
end

#Assign colors, assign exisiting if present else give a new color
gene_names.each do |gn|
  if existing_colors.has_key?(gn)
    gene_colors[gn] = existing_colors.fetch(gn)
  else
    gene_colors[gn] = colors.shift
  end
end

#Write updated $COLOR_LIST
puts "Colors assigned. Writing new color list!" if $DEBUG == true
color_file = File.open($COLOR_LIST, "w")
gene_colors.each {|gene, color| color_file.write("#{gene}::#{color}\n")}
color_file.close


#*****************************************************
#Step4: IMAGE GENERATION

canvas = Magick::Image.new(img_width, img_height) { self.density=$DPI}
canvas.resample($DPI)
canvas.units = Magick::PixelsPerInchResolution
canvas.density = $DPI.to_s

#Draw Genome Names
final_contig_structs.collect {|c| c.disp_y}.uniq.each do |level_y|
  mark = Magick::Draw.new
  mark.font_style = ItalicStyle
  mark.pointsize = genome_name_pt_size
  mark.font_family = "Arial"
  mark.font_weight = BoldWeight
  mark.text(15, level_y + (0.7 * contig_height), genome_names[final_contig_structs.find{|c| c.disp_y == level_y}.genome_number])
  mark.draw(canvas)
end

#Gather Connection Coordinates and Draw Contig shadows if set
gene_coords = Array.new # container of coord blocks for gene markers [name, top-x,top-y, bottom-x,bottom-y,dir]
final_contig_structs.each_index do |c|
  draw_contig_dropshadow(final_contig_structs[c], end_stype='jagged').draw(canvas) if draw_dropshadow == true
  cgenes = gene_structs.find_all {|g| g.contig == final_contig_structs[c].name && g.genome_number == final_contig_structs[c].genome_number}
  
  #Collect coords
  cgenes.each {|g| 
    if $SHADE_HITS == 'blast'
      gx_start = convert_range(x=g.bp_seg_start, final_contig_structs[c].bp_end, final_contig_structs[c].bp_start,(final_contig_structs[c].disp_x + final_contig_structs[c].width), final_contig_structs[c].disp_x)
      gx_end = convert_range(x=g.bp_seg_end, final_contig_structs[c].bp_end, final_contig_structs[c].bp_start,(final_contig_structs[c].disp_x + final_contig_structs[c].width), final_contig_structs[c].disp_x)
    else
      gx_start = convert_range(x=g.bp_start, final_contig_structs[c].bp_end, final_contig_structs[c].bp_start,(final_contig_structs[c].disp_x + final_contig_structs[c].width), final_contig_structs[c].disp_x)
      gx_end = convert_range(x=g.bp_end, final_contig_structs[c].bp_end, final_contig_structs[c].bp_start,(final_contig_structs[c].disp_x + final_contig_structs[c].width), final_contig_structs[c].disp_x)
    end
    gx_width = gx_end - gx_start + 1
    gene_coords << [g.name, gx_start + (0.5 * gx_width), final_contig_structs[c].disp_y, gx_start + (0.5 * gx_width), (final_contig_structs[c].disp_y + final_contig_structs[c].height), gx_width, g.dir] # x start/end adjusted to have lines start/stop at midpoints
  }
end

#Draw connections
$last_x = 1 # holds [1,-1] to move arrows up/down
puts "Drawing connections between genes using connection style: #{$CONNECT_STYLE}." if $DEBUG == true
draw_connections(gene_coords, gene_colors, level_markers, contig_height, img_height, img_width, type=$CONNECT_STYLE, mode='draw', dropshadow=draw_dropshadow, linetype=$LINETYPE).draw(canvas)

#Draw contig foreground and gene markers
$last_x = 0 #reset x to far left
final_contig_structs.each_index do |c|
  cgenes = gene_structs.find_all {|g| g.contig == final_contig_structs[c].name && g.genome_number == final_contig_structs[c].genome_number}
  puts "Drawing contig #{final_contig_structs[c].name} with #{cgenes.length} genes." if $DEBUG == true
  
  #Draw Contigs
  draw_contig(final_contig_structs[c]).draw(canvas)
  
  #Draw Gene Markers and collect coords
  gcount = 0
  cgenes.each {|g| draw_gene(final_contig_structs[c], g.name, g.bp_start, g.bp_end, g.bp_seg_start, g.bp_seg_end, arrow_width, gene_colors[g.name], g.dir, gcount, 'full').draw(canvas); gcount += 1}
end

canvas = canvas.quantize(256, Magick::GRAYColorspace) if $GRAYSCALE_MODE == true
canvas.write($SAVED_IMAGE_NAME)
puts "Finished. Saved image '#{$SAVED_IMAGE_NAME}' in directory: #{Dir.pwd}"
exit

#==================================================
#END PROGRAM
#==================================================
