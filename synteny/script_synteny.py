#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import pandas as pd
import numpy as np
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
import sys 
#______________________________________________________________________________________________________
#script_synteny.py
#script permettant de réaliser la syntenie d'une region en utilisant une liste de gene

#commande : python script_synteny.py doss_contig/ seq_gene doss_result/ 

#doss_contig/ : dossier contenant les contigs 
#seq_gene : fichier fasta des gènes dont on souhaite faire la synthenie
#doss_result/ : dossier resultat (Synthenie)

#______________________________________________________________________________________________________
# fonction qui indexe les fichiers fasta des contigs donnees 
def indexation(doss_cont):
	ls=os.listdir(doss_cont)
	for i in ls : 
		if i[-5:]=='fasta' or i[-3:]=='fna':
			name_cont =doss_cont+i
			os.system('makeblastdb -in ' + name_cont + ' -out ' + name_cont + ' -dbtype nucl')
	return


#______________________________________________________________________________________________________
# fonction realisant le blast entre les sequence query et notre liste de contigs
def blast_synteny (doss_contig, gene, doss_result):
	evalue=1e-2
	ls=os.listdir(doss_contig)  

	cont=[]
	for i in ls :
		if i[-5:]=='fasta' or i[-3:]=='fna':
			cont.append(i)

	for i in cont : 
		name_cont = doss_contig+i
	
		name_out = i.split('.')[0]+'.bln'
		print('blast en cours pour', name_cont)
		os.system ( 'blastn -task blastn -query '+ gene +' -db ' + name_cont + ' -out '+ doss_result + name_out + ' -outfmt "6 qseqid sseqid length qlen slen qcovs qstart qend sstart send sacc score evalue " -evalue '+str(evalue))
		print('blast terminé pour ', name_cont,'\n')

	return
#______________________________________________________________________________________________________
# fonction qui va attribuer pour chaque colonne du fichier de sortie blast un titre
def rajout_titre(ndoss, file_bln):
    titre_col = ['query id','subject id','alignment length','query length','subject length','% query coverage per subject','q. start','q. end','s. start','s. end','subject acc.','score','evalue']
    blast_tab = pd.read_csv(ndoss+file_bln,sep='\t')
    if titre_col[0] not in list(blast_tab.columns) :
        blast_tab = pd.read_csv(ndoss+file_bln,sep='\t',header=None)
        blast_tab.columns = titre_col
        blast_tab.to_csv(ndoss+file_bln,sep ='\t',index=False)
    return

#______________________________________________________________________________________________________
# fonction permettant de generer des fichier cmap
def cmap_file (doss_blast,doss_cmap):
    ls_bln=os.listdir(doss_blast)
    for i in ls_bln:
        rajout_titre(doss_blast,i)
        blast_tab = pd.read_csv(doss_blast+i,sep='\t')
        ligne=[]
        ls_g = []
        for j in list(set(blast_tab['query id'])):
            t=blast_tab[blast_tab['query id']==j]
            t=t.sort_values(by = ['score'],ascending=[False])
            ligne.append(list(t.index)[0])
    
        new_tab=blast_tab.loc[ligne,:]
        new_tab=new_tab.sort_values(by = ['s. start'],ascending=[True])
    
        gen=i.split('.')[0]
        chaine=gen+' {1  '
        for k in new_tab.index:
            ss = new_tab['s. start'][k]
            se= new_tab['s. end'][k]
            name= new_tab['query id'][k]
            qs= new_tab['q. start'][k]
            qe= new_tab['q. end'][k]
            ls_g.append(name)
            if ss>se :
                sens ='  <== '+name+'  '
            else :
                sens = '  '+name+' ==>  '
            gene='[ '+str(ss)+'|'+str(ss)+' ('+str(qs)+'|'+str(qs)+sens+str(qe)+'|'+str(qe)+') '+str(se)+'|'+str(se)+' ]  '
            chaine+=gene
        chaine+= str(se)+'}'
        nameFile=doss_cmap+i.split('.')[0]+'.cmap'
        f=open(nameFile,'w')
        f.write(chaine)
        f.close()
    return
#______________________________________________________________________________________________________
# MAIN
def main():
    doss_contig = sys.argv[1]
    gene = sys.argv[2]
    doss_result = sys.argv[3]
    
    doss_result_blast = doss_result+'blast_synteny/'
    doss_result_cmap = doss_result+'cmap_synteny/'
    for i in [doss_result_blast,doss_result_cmap] : 
	    os.makedirs(i, exist_ok=True)

    indexation(sys.argv[1])

    print('1er etape : Blast\n')
    blast_synteny(sys.argv[1],sys.argv[2],doss_result_blast)
    print('2eme etape : Generation des fichier CMAP\n')
    cmap_file (doss_result_blast,doss_result_cmap)

if __name__ == '__main__':
    main()
