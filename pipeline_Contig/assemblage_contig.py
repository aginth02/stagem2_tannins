#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import os
import sys 
from Bio import SeqIO
from collections import OrderedDict
pd.options.mode.chained_assignment = None  # default='warn'
from recuperation_contig import *
#___________________________________________________________________________________________________________
#
# SCRIPT CONTENANT TOUTES LES FONCTIONS PERMETTANT L'ASSEMBLAGE DES CONTIGS 
#
#___________________________________________________________________________________________________________

# fonction pemettant de simplifier le fichier blast des contigs. 
# Lorsque que query id = subject id ; alors cette ligne est supprime
# entree : fichier blast -> sortie : tableau blast

def simplifie_blast(ndoss, file_csv):
    blast_tab = pd.read_csv(ndoss+file_csv,sep='\t')
    if len(set(blast_tab['query id']))<2 :
        a=0
        return blast_tab, a
    else : 
        a=1
        ls = list(set(blast_tab['query id']))
        for i in ls :
            #print(i)
            i_q = blast_tab[blast_tab['query id']==i].index
            i_s = blast_tab[blast_tab['subject id']==i].index
            com = list(set(i_q)&set(i_s))
            #On va eliminer les ligne ou query id = subject id 
            blast_tab.drop(com,inplace=True) 
            
    return blast_tab, a

#___________________________________________________________________________________________________________

# fonction permettant de récuper un dico entre les contigs qui s'assemble entre eux
# ainsi que leur coordonnées
# entree : tableau blast + tableau best contig -> sortie : dico qui pour chaque couple
# de contig donne leur coordonnées pour lesquelle nos 2 contig 'match'

def coord_assemblage(df_best_contig,blast_tab_contig):
    blast_tab_contig['score_reduit'] = blast_tab_contig['score']/blast_tab_contig['query length']
    blast_tab_contig['taille'] = abs(blast_tab_contig['q. end']-blast_tab_contig['q. start'])
    cont = list(df_best_contig.index)
    dico_assemblage = dict()
    for i in range(len(cont)) :
        if i != len(cont)-1:
            cont1 = cont[i] ; cont2 = cont[i+1]
            new_tab = blast_tab_contig[blast_tab_contig['query id']==cont1]
            new_tab = new_tab[new_tab['subject id']==cont2]
            new_tab = new_tab.sort_values(by = ['score_reduit','alignment length'],ascending=[False,False])
            
            index=list(new_tab.index)[0]
            
            for j in list(new_tab.index) :
                coord1 =int(new_tab['q. start'][j]) ; coord2 = int(new_tab['q. end'][j])
                start_cont = int(df_best_contig['start_cont'][cont1]) ; end_cont = int(df_best_contig['end_cont'][cont1])
                Min = min(start_cont,end_cont) ; Max = max(end_cont,start_cont)
                
                if (coord1 in range(Min,Max)) or (coord2 in range(Min,Max)) :
                    index = j
                    break
                
            
            coord_cont1 = [int(new_tab['q. start'][index]),int(new_tab['q. end'][index])]
            coord_cont2 = [int(new_tab['s. start'][index]),int(new_tab['s. end'][index])]
            cle = (cont1,cont2)
            dico_assemblage[cle]=[coord_cont1,coord_cont2]
                    
    return dico_assemblage
 
#___________________________________________________________________________________________________________

# fonction qui retourne un tableau conteant comme information les coordonées du contig,
# la presence ou non d'un trou et le sens
# entree : tableau best contig + dico assemblage -> sortie : tableau avec pour chaque contig
# leur coordonnees, leur sens ainsi que la presence d'un gap
 
def assemblage(df_best_contig,dico_assemblage):
    cle = list(dico_assemblage.keys())
    df_coord = pd.DataFrame(columns =['coord1','coord2','trou','frame'],index=list(df_best_contig.index))
    for i in cle :
        coord_am = dico_assemblage[i][0] ; coord_av = dico_assemblage[i][1]
        cont_am = i[0] ; cont_av = i[1]
        
        df_coord['frame'][cont_am]=df_best_contig['frame'][cont_am]
        df_coord['frame'][cont_av]=df_best_contig['frame'][cont_av]
        
        if abs(coord_am[1] - coord_am[0]) <= 12000 : 
            df_coord['trou'][cont_am] = 1
            
            if df_coord['frame'][cont_am] == '-':
                df_coord['coord2'][cont_am] = 0
            else :
                df_coord['coord2'][cont_am] = df_best_contig['taille contig'][cont_am]+1
                
            if df_coord['frame'][cont_av] == '-':
                df_coord['coord1'][cont_av] = df_best_contig['taille contig'][cont_av]+1
            else :
                df_coord['coord1'][cont_av] = 0
            
        else :
            
            df_coord['trou'][cont_am] = 0
            if df_coord['frame'][cont_am] == '-':
                df_coord['coord2'][cont_am]=coord_am[0]+1
            else :
                df_coord['coord2'][cont_am]=coord_am[1]+1
                
                
            if df_coord['frame'][cont_av] == '-':
                df_coord['coord1'][cont_av] = coord_av[0]+1
            else :
                df_coord['coord1'][cont_av] = coord_av[1]+1
               
        # On determine la valeur de la coordonnées 2 pour notre dernier contig
        if i == cle[-1]:
            if df_coord['frame'][cont_av] == '+' :
                df_coord['coord2'][cont_av] = df_best_contig['taille contig'][cont_av]
                
            else :
                df_coord['coord2'][cont_av] = 0 
        # On determine la valeur de la coordonnées 1  pour notre 1er contig    
        if i == cle[0]:
            if df_coord['frame'][cont_am] == '+' :
                df_coord['coord1'][cont_am] = 0
            else : 
                df_coord['coord1'][cont_am]= df_best_contig['taille contig'][cont_am]
    
    return df_coord

#___________________________________________________________________________________________________________

# fonction qui va creer un fichier fasta avec les assemblages des contigs
# entree : tableau contig -> sortie : fichier fasta de l'assemblage 

def ecriture_assemblage(file1,df_coord,doss):
    dico_seq=SeqIO.to_dict(SeqIO.parse(file1,'fasta'))
    name1=doss+'/assemblage_'+file1.split('/')[-1]
    file_assem = open(name1,'w')
    ls_cont = list(df_coord.index)
    assemblage = ''
    asbl_id =''

    for i in ls_cont :
        coord1 = int(df_coord['coord1'][i])
        coord2 = int(df_coord['coord2'][i])

        #Recuperation des noms des contigs
        Id = dico_seq[i].id
        if 'Haplotig' in Id :
            Id = Id.split('_')[-2:]
            Id = "".join(Id)
        else :
            Id = Id.split('_')[-1]
        
        
        #recupération des contigs
        if df_coord['frame'][i] == '+':
            h = dico_seq[i][coord1:coord2]
            
        else :
            h = dico_seq[i][coord2:coord1]
            h = h.reverse_complement()
            Id = Id+'_RC'
        #assemblage des contigs
        if df_coord['trou'][i]==1:
            N=1000*'N'
            assemblage += h +N

        else :
            assemblage += h

        asbl_id += Id + '___'    
    assemblage.id = asbl_id
    file_assem.write(assemblage.format('fasta'))
    file_assem.close()

    return assemblage  

#___________________________________________________________________________________________________________

# fonction qui pour les genomes ne possédant qu'un contig intéréssant, va retourner l'inverse complementaire
# du contig si celui-ci était orienté négativement
# entrée : tableau best contig -> sortie : fichier fasta du contig  

def reverse_contig(file1,df_best_contig,doss):
    bt_cont = list(df_best_contig.index)[0]

    if df_best_contig['frame'][bt_cont] == '-' :
        
        dico_seq=SeqIO.to_dict(SeqIO.parse(file1,'fasta'))
        name1=doss+'/Rc_contig_'+file1.split('/')[-1]
        file_contig_RC = open(name1,'w')
        h = dico_seq[bt_cont]
        ID=h.id
        z = h.reverse_complement()
        z.id = ID
        z.name = ID
        file_contig_RC.write(z.format('fasta'))
        file_contig_RC.close()
    return 