#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os 
import sys 

def indexation(doss_banque):
	ls=os.listdir(doss_banque)
	for i in ls : 
		if i[-5:]=='fasta' or i[-3:]=='fna':
			name_gen =doss_banque+i
			os.system('makeblastdb -in ' + name_gen + ' -out ' + name_gen + ' -dbtype nucl')
	return


def blast_genome(query,doss_result,doss_banque):
	evalue=1
	ls=os.listdir(doss_banque) # doss banque

	gen=[]
	for i in ls :
		if i[-5:]=='fasta' or i[-3:]=='fna':
			gen.append(i)

	for i in gen : 
		name_gen = doss_banque+i
	
		name_out = i.split('.')[0]+'.bln'
		print('blast en cours pour', name_gen)
		os.system ( 'blastn -task blastn -query '+ query +' -db ' + name_gen + ' -out '+ doss_result + name_out + ' -outfmt "6 qseqid sseqid length qlen slen qcovs qstart qend sstart send sacc score evalue" -evalue '+str(evalue))
		print('blast terminé pour ', name_gen,'\n')

	return 


def blast_contig(doss_result,doss_banque):
	evalue=10
	ls=os.listdir(doss_banque)
	gen=[]
	for i in ls :
		if i[-5:]=='fasta' or i[-3:]=='fna':
			gen.append(i)
	if len(gen)>1:
		for i in gen : 
			name_gen = doss_banque+i
			name_out = i.split('.')[0]+'.bln'
			print('blast en cours pour', name_gen)
			os.system ( 'blastn -task megablast -query '+ name_gen +' -db ' + name_gen + ' -out '+ doss_result + name_out + ' -outfmt "6 qseqid sseqid length qlen slen qcovs qstart qend sstart send sacc score evalue" -evalue '+str(evalue))
			print('blast terminé pour ', name_gen,'\n')
	else :
		i=gen[0]
		name_gen = doss_banque+i
		name_out = i.split('.')[0]+'.bln'
		print('blast en cours pour', name_gen)
		os.system ( 'blastn -task blastn -query '+ name_gen +' -db ' + name_gen + ' -out '+ doss_result + name_out + ' -outfmt "6 qseqid sseqid length qlen slen qcovs qstart qend sstart send sacc score evalue" -evalue '+str(evalue))
		print('blast terminé pour ', name_gen,'\n')

	return 


