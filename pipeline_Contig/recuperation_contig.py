#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import os
import sys 
from Bio import SeqIO
from collections import OrderedDict
pd.options.mode.chained_assignment = None  # default='warn'

#______________________________________________________________________________________________________________________________
#
# SCRIPT CONTENANT TOUTES LES FONCTIONS POUR LA RECUPERATION DE CONTIGS 
#______________________________________________________________________________________________________________________________

#fonction permettant de recuperer nos fichiers fasta ainsi que nos fichiers bln
# entree : dossier contenant les fichier fasta et bln --> liste des fichier contenu dans ces dossiers 

def recuperation_fich (doss1,doss2):
    ls_fasta=os.listdir(doss1)
    ls_bln = os.listdir(doss2)
    for i in ls_bln :
        if i[-3:]=='bln':
            new_blnfile = rajout_titre(doss2,i)
            
    fasta_file=[]
    for i in ls_fasta :
        if i[-5:]=='fasta' or i[-3:]=='fna':
            fasta_file.append(i)



    bln_file=[]
    for i in ls_bln :
        if i[-3:]=='bln':
            bln_file.append(i)

        
    dico_file=dict()
    for i in bln_file:
        for j in fasta_file:
            if i[:-3] in j:
                dico_file[i]=j
                
    return fasta_file, bln_file, dico_file

#______________________________________________________________________________________________________________________________
# fonction permettant d'ajouter les titres des colonnes pour nos sortie blast
# entree : fichier bln --> sortie : fichier bln (avec titre de colonne)

def rajout_titre(ndoss, file_bln):
    titre_col = ['query id','subject id','alignment length','query length','subject length','% query coverage per subject','q. start','q. end','s. start','s. end','subject acc.','score','evalue']

    blast_tab = pd.read_csv(ndoss+file_bln,sep='\t')

    if titre_col[0] not in list(blast_tab.columns) :
        blast_tab = pd.read_csv(ndoss+file_bln,sep='\t',header=None)
        blast_tab.columns = titre_col
        blast_tab.to_csv(ndoss+file_bln,sep ='\t',index=False)
    return 

#______________________________________________________________________________________________________________________________
# fonction permettant de determiner le rang de chaques contigs pour chacunes de nos sondes
# entree : tableau blast --> sortie : tableau blast trie selon le rang 

def rang_hit (blast_tab):
    start_sonde1 = 16280828
    end_sonde_fin = 18828603
    
    # On rajoute 4 colonnes : 
    # rang = 
    # sonde rang = rang de la sonde en fonction du score reduit
    # aval_scaff = longueur du scaffold en aval de la derniere sonde
    # amont_scaff = longueur du scaffold en ammont de la premiere sonde 
    
    my_cols_list=['rang','sonde rang','aval_scaff','amont_scaff']
    blast_tab = blast_tab.reindex(columns=[*blast_tab.columns.tolist(),*my_cols_list])
    
    # On trie notre tableau en fonction des sondes et des scores reduit decroissants
    blast_tab = blast_tab.sort_values(by = ['query id','ratio'],ascending=[True,False])
    nb_gene=list(set(blast_tab['query id']))
    
    # 1er etape on va determiner le rang de chaque sonde pour tous les subjects id  
    for i in nb_gene :
        ind = list(blast_tab[blast_tab['query id']==i].index)
        c=1
        for j in ind :
            blast_tab['rang'][j]=c
            c+=1     
        #length = len(blast_tab[blast_tab['query id']==i])+1
        #blast_tab['rang'][ind]=np.arange(1,length)
    
    # 2eme etape on garde que les sondes de meilleurs choix (4eme choix au maximum)
    for i in nb_gene :
        ind = list(blast_tab[blast_tab['query id']==i].index)
        for j in range(len(ind)) :
            if j==0:
                blast_tab['sonde rang'][ind[j]]=1
            else :
                if blast_tab['ratio'][ind[j]] == blast_tab['ratio'][ind[j-1]] :
                    blast_tab['sonde rang'][ind[j]]=blast_tab['rang'][ind[j-1]]
                else :
                    blast_tab['sonde rang'][ind[j]]=blast_tab['rang'][ind[j]]
    
    # On trie en fonction du rang des sondes et du qstart_region en fonction croissante 
    blast_tab = blast_tab.sort_values(by = ['sonde rang','qstart_region'],ascending=[True,True])
    blast_tab = blast_tab[blast_tab['sonde rang']<=4]
    
    # On calcule la différence entre s. end et s. start et on recupere les index quand elles sont negatifs
    # et quand elle sont positifs
    scaff = blast_tab['s. end']-blast_tab['s. start']
    i_pos = list(scaff[scaff>0].index) ; i_neg = list(scaff[scaff<0].index)
    
    # On calcule la longueur du scaffold en ammont de la 1er sonde et en aval de la dernière sonde
    for i in i_pos :
        blast_tab['amont_scaff'][i]=int(blast_tab['s. start'][i])-int(blast_tab['qstart_region'][i])+start_sonde1
        blast_tab['aval_scaff'][i]= int(blast_tab['subject length'][i])-int(blast_tab['s. end'][i])-end_sonde_fin+int(blast_tab['qend_region'][i])
        
    for i in i_neg :
        blast_tab['amont_scaff'][i]=int(blast_tab['subject length'][i])-int(blast_tab['s. end'][i])-int(blast_tab['qstart_region'][i])+start_sonde1
        blast_tab['aval_scaff'][i]= int(blast_tab['s. start'][i])-end_sonde_fin+int(blast_tab['qend_region'][i])

    # On creer un nouveau fichier avec le nouveau tableau 
    #blast_tab.to_csv("new_tab_rang2.csv",sep ='\t')

    return blast_tab

#______________________________________________________________________________________________________________________________
# fonction permettant un tri de notre tableau blast 
# entree : fichier bln --> sortie : tableau blast trie

def tri_blast(ndoss, file_csv):
    blast_tab = pd.read_csv(ndoss+file_csv,sep='\t')
    blast_tab = blast_tab.sort_values(by = ['subject id','query id', 'q. start', 'q. end'])
    
    #filtrage du blast 
    blast_tab['ratio'] = blast_tab['score']/blast_tab['query length']
    blast_tab = blast_tab.sort_values(by = ['ratio'],ascending=False)
    blast_tab[['query id','region coord']] = blast_tab['query id'].str.split("::",expand=True,)
    blast_tab[['chr','region coord']] = blast_tab['region coord'].str.split(":",expand=True,)
    blast_tab[['qstart_region','qend_region']]=blast_tab['region coord'].str.split("-",expand=True,)
    del(blast_tab['chr'])
    del(blast_tab['region coord'])
    blast_tab=blast_tab[['query id','subject id','alignment length','query length','subject length','% query coverage per subject','q. start','q. end','qstart_region','qend_region','s. start','s. end','subject acc.','score','evalue','ratio']]
    blast_tab = rang_hit(blast_tab)
    
    return blast_tab


#______________________________________________________________________________________________________________________________
# fonction permettant une analyse de notre tableau blast. Pour chaques contigs, on récupere le nombre de hits, le nombres de 
# sondes présente et la longueurs du contig
# entree : tableau blast trie --> sortie : nouveau tableau avec les infos pour chaques contigs 

def analyse_blast(blast_tab):
    d_chr=dict()
    ls_contig = list(set(blast_tab['subject id']))
    for i in ls_contig :
        grouped = blast_tab.groupby('subject id')
        new_tab = grouped.get_group(i)
        nb_hit = len(new_tab)
        nb_gene = list(set(new_tab['query id']))
        taille_s = new_tab['subject length'].values[0]
        d_chr[i]=[nb_hit,len(nb_gene),taille_s]
  
    df_chr = pd.DataFrame.from_dict(d_chr, orient='index',columns=['nb hits','nb gene present','taille contig'])
    df_chr = df_chr.sort_values(by = ['nb hits'],ascending=False)
    df_chr = df_chr.loc[list(df_chr.index),:]
        
    return df_chr

#______________________________________________________________________________________________________________________________
# fonction permettant de recuperer les meilleures contigs ainsi que leur informations
# entree : tableau blast trie + tableau info contigs --> sortie : tableau avec meilleurs contigs 

def recup_contigv2(df_chr,blast_tab):
    df_chr = df_chr.sort_values(by = ['nb hits','nb gene present','taille contig'],ascending=[False,False,True])
    ls_contig = list(df_chr.index)
    
    nb_gene=list(set(blast_tab['query id']))
    
    df_contig=pd.DataFrame(np.zeros((len(nb_gene),len(ls_contig))),columns=ls_contig,index=nb_gene)
    df_contig['qstart_region']=np.zeros((len(nb_gene),1))
    df_contig['qend_region']=np.zeros((len(nb_gene),1))
    
    new_tab = blast_tab.loc[blast_tab['subject id'].isin(ls_contig)]
    new_tab = new_tab.sort_values(by = ['ratio','qstart_region','qend_region'],ascending=[False,True,True])
    
    
    for i in new_tab.index :
        gene = new_tab['query id'][i]
        contig = new_tab['subject id'][i]
        qstart = new_tab['qstart_region'][i]
        qend = new_tab['qend_region'][i]
        df_contig[contig][gene]=1
        df_contig['qstart_region'][gene]=qstart
        df_contig['qend_region'][gene]=qend
        
    df_contig = df_contig.sort_values(by = ['qstart_region',],ascending=True)
    
    
    a=0
    best_contig=[]
    for i in df_contig.columns[:-2] :
        if a == 0:
            d=df_contig[i]
            best_contig.append(i)
            a+=1
            
        else :
            
            z = list(d[d==0].index)
            if len(z)>0 :
                r=df_contig[i]
                e=list(r[r==1].index)
                commun=list(set(e)&set(z))
                if len(commun)>0:
                    best_contig.append(i)
                    d[commun]=1
            else :
                break
                
    df_best_contig = df_chr.loc[best_contig,:]
    
    
    if df_best_contig['nb gene present'].max() >= 100 :
        df_best_contig = df_best_contig[df_best_contig['nb gene present']>=100]
    else :
        df_best_contig = df_best_contig[df_best_contig['nb gene present']>=4]
        
    df_best_contig = coord_contig(df_best_contig,blast_tab)
    
    return df_best_contig

#______________________________________________________________________________________________________________________________
# Fonction permettant de determiner le frame de notre contigs
# entree : tableau blast -> sortie : orientation du contig et tableau_blast 

def frame(new_tab):
    index = list(new_tab.index)
    new_tab['frame sonde'] = ''
    for i in range(0,len(index)):
        if new_tab['s. end'][index[i]]-new_tab['s. start'][index[i]]<0:
            new_tab['frame sonde'][index[i]] = '-'
        else : 
            new_tab['frame sonde'][index[i]] = '+'
    
    ls_p = list(new_tab[new_tab['frame sonde']=='+'].index)
    new_tab_pos = new_tab.loc[ls_p,:]
    l_p = len(list(set(new_tab_pos['query id'])))  # nb de sondes orienté positivement
    
    
    ls_m = list(new_tab[new_tab['frame sonde']=='-'].index)
    new_tab_neg = new_tab.loc[ls_m,:]
    l_m = len(list(set(new_tab_neg['query id'])))  # nb de sondes orienté négativement 
    
    
    if l_p>l_m:
        frame = '+'
    else :
        frame = '-'
        
    new_tab = new_tab[new_tab['frame sonde']==frame]
    new_tab = new_tab.sort_values(by = ['qstart_region'],ascending=[True])
    return frame,new_tab

#______________________________________________________________________________________________________________________________
# Fonction permettant de determiner l'ordre des contigs
# entree : tableau best contig -> 

def ordre(df_best_contig):
                   
    df_best_contig = df_best_contig.sort_values(by = ['end_region','start_region'],ascending=[True,True])
    df_best_contig['ordre']=''
    if len(df_best_contig.index) > 1 :
        l_index = list(df_best_contig.index)
        for i in range(len(l_index)):
            if i == 0 :
                ordre = 1 
                df_best_contig['ordre'][l_index[i]]=ordre 
            else :
                ordre+=1
                df_best_contig['ordre'][l_index[i]]=ordre 

    return df_best_contig


#______________________________________________________________________________________________________________________________
# fonction permettant de récuperer les coordonees des contigs que l'on a selectionne
# entree : tableau avec meilleurs contigs --> sortie : tableau avec meilleurs contigs + coordonnees de ses contigs

def coord_contig(df_best_contig,blast_tab) :
    my_cols_list=['start_region','end_region','start_cont','end_cont','frame']
    df_best_contig = df_best_contig.reindex(columns=[*df_best_contig.columns.tolist(),*my_cols_list])
    
    contig = list(df_best_contig.index)
    for i in contig :
        grouped = blast_tab.groupby('subject id')
        new_tab = grouped.get_group(i)
        new_tab = new_tab.sort_values(by = ['qstart_region'],ascending=[True])
        
        df_best_contig['frame'][i],new_tab = frame(new_tab)
        index=list(new_tab.index)

        df_best_contig['start_region'][i] = new_tab['qstart_region'][index[0]]
        df_best_contig['end_region'][i] = new_tab['qend_region'][index[-1]]
        
        df_best_contig['start_cont'][i] = new_tab['s. start'][index[0]]
        df_best_contig['end_cont'][i] = new_tab['s. end'][index[-1]]
        
    df_best_contig = ordre(df_best_contig)
    return df_best_contig

#______________________________________________________________________________________________________________________________
# fonction permettant de creer un fichier fasta avec la sequence de nos contigs selectionne
# entree : repertoire , fichier fasta du genome complet et tableau meilleur contig --> sortie : fichier fasta avec nos contigs 

def ecriture_contig(file1, df_best_contig ,doss):
    dico_seq = SeqIO.to_dict(SeqIO.parse(file1, 'fasta'))
    name1=doss+'/'+'contig_'+file1.split('/')[-1]
    file_contig=open(name1,'w')
    ls_cont=list(df_best_contig.index)
    for i in ls_cont:
        h = dico_seq[i]
        file_contig.write(h.format('fasta'))

    return ('fichiers prêts')
