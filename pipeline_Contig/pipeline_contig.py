#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import os
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from collections import OrderedDict
pd.options.mode.chained_assignment = None  # default='warn'
from recuperation_contig import *
from assemblage_contig import *
from fonction_blast import *

#_________________________________________________________________
# Ce projet est composé de 3 script principale :
# Le script pipeline_contig est une pipeline permettant de recuperer et assembler des contigs encadrant une region f interet. La pipeline necessite 3 argument :
# Doss_resultat : dossier ou vont se trouver les resultat de notre pipeline 

# Query : represrntr le fichier fasta comprenany nos sondr 

# Database : dossier regroupant tout nos génome

# Pour lancer le script il suffit de lancer une commande de la maniere suivante 

# Python pipeline_contig.py /.../name_dossResult /.../name_query.fasta /.../name_dossDatabase


#_________________________________________________________________
# ARGUMENTS


result = sys.argv[1]
database = sys.argv[2]
query = sys.argv[3]

#_________________________________________________________________
# Creation des dossiers  

doss_contig = result + 'Contig/'
doss_bestgenome = doss_contig + 'best_genome/'
doss_contig2 =  doss_contig + 'contig/'
doss_Assemblycontig = doss_contig + 'Assembly_contig/'


doss_bln = result + 'Bln_file/'
doss_bln_gen = doss_bln + 'bln_genome/'
doss_bln_cont= doss_bln + 'bln_contig/'

doss_mapping = result + 'Mapping/'
doss_RepMask = result + 'RepeatMasker/'
doss_synteny = result + 'Synteny/'

ls_doss = [doss_contig,doss_bestgenome,doss_contig2,doss_Assemblycontig,
doss_bln,doss_bln_gen,doss_bln_cont,doss_mapping,doss_RepMask,
doss_synteny]

for i in ls_doss : 
	os.makedirs(i, exist_ok=True)

#_________________________________________________________________
# 1er etape : Creation des banques et blast 

print('INDEXATION DE LA BANQUE ...')
indexation(database)
print('INDEXATION TERMINE \n')

print('DEBUT BLAST ...')
blast_genome(query,doss_bln_gen,database)
print('FIN BLAST \n')

#_________________________________________________________________
# 2eme etape : Récupération des contigs

print('RECUPERATION DES CONTIGS ...')
fasta_file, bln_file, dico_file = recuperation_fich (database,doss_bln_gen)
dico_df = dict()
print(bln_file)
for i in bln_file :
	
	blast_tab = tri_blast(doss_bln_gen,i)
	df_chr = analyse_blast(blast_tab)
	df_best_contig = recup_contigv2(df_chr,blast_tab)
	dico_df[i]=df_best_contig

	if len(df_best_contig.index)>1:
		fich_fasta = database + dico_file[i]   
		ecriture_contig(fich_fasta,df_best_contig,doss_contig2)
		
	else :
		fich_fasta = database + dico_file[i]
		ind = list(df_best_contig.index)[0]
		if df_best_contig['frame'][ind]=='-':
			rev_cont = reverse_contig(fich_fasta,df_best_contig,doss_bestgenome)
		else :
			ecriture_contig(fich_fasta,df_best_contig,doss_bestgenome)

print('FIN RECUPERATION DES CONTIGS \n')

#________________________________________________________________
# 3eme etape : Blast des contig + assemblage 

print('ASSEMBLAGE DES CONTIGS ...')
indexation(doss_contig2)
blast_contig(doss_bln_cont,doss_contig2)
ls_bln_contig = os.listdir(doss_bln_cont)

dico_contig = dict()
for i in bln_file :
	for j in ls_bln_contig :
		if i[:-4] in j :
			dico_contig[i]=j

print(dico_contig)


for i in bln_file:
	df_best_contig=dico_df[i]
	if len(df_best_contig.index)>1:
		rajout_titre(doss_bln_cont, dico_contig[i])
		bln_contig = dico_contig[i]
		bt_contig, a = simplifie_blast(doss_bln_cont, bln_contig) 
		dico_assemblage = coord_assemblage(df_best_contig,bt_contig)
		df_coord = assemblage(df_best_contig,dico_assemblage)

		fich_fasta = database + dico_file[i]
		asbl = ecriture_assemblage(fich_fasta,df_coord,doss_Assemblycontig)
print('FIN ASSEMBLAGE DES CONTIGS \n')

#________________________________________________________________






